<?php get_header(); ?>
<?php global $ids; ?>
<script type="text/javascript">
    var ruta = '<?php bloginfo('template_url'); ?>';
</script>
<div class="container main-content-fotos">
    <div class="row">
        <div id="fotos-wrapper" class="col-md-12 col-xs-12 col-sm-12 padding-xs contenido no-paddingl no-paddingr">
            <?php remove_filter('the_content', 'wpautop'); remove_filter('the_excerpt', 'wpautop');?>
            <?php query_posts( array ( 'category_name' => 'fotos', 'posts_per_page' => -1,  'orderby' => 'date', 'order' => 'DESC' ) ); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php $ids[] = get_the_ID(); ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
            <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php $n_posts  = count($ids); $z == 0; ?>
            <?php while ($enc == 0):
if ($postid == $ids[$z]){
    if ($z == 0){
        $anterior = $ids[0]; $siguiente = $ids[$z+1];
        $actual = $ids[$z];
    } else {
        $anterior = $ids[$z-1]; $siguiente = $ids[$z+1];
        $actual = $ids[$z];
    }
    if ($z === $n_posts){
        $anterior = $ids[$z-1]; $siguiente = $ids[0];
    }
    $enc = 1;
}
$z++;
endwhile; ?>
            <div class="col-md-1"><a id="<?php echo $anterior; ?>" href="#" onclick="prev(this.id);" class="inactivo"><i class="fa fa-angle-left fa-5x"></i></a></div>
            <?php get_post( $actual ); ?>
            <?php echo get_the_ID(); ?>
            <?php $post_thumbnail_id = get_post_thumbnail_id(); ?>
            <?php $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, 'full' ); ?>
            <?php $width = $image_attributes[1]; ?>
            <?php $height = $image_attributes[2]; ?>
            <?php if ($width > $height){ $clase = "archive-ancho-fotos";} else { $clase = "archive-largo-fotos"; } ?>
            <article itemscope itemtype="http://schema.org/NewsArticle">
                <div class="col-md-10 col-xs-6 col-sm-6">

                    <a href="<?php the_permalink() ?>">
                        <div class="col-md-12 col-xs-12 col-sm-12 archive-fotos-container">
                            <?php $pic = get_post_meta(get_the_ID(), 'sum_e_url', true); ?>
                            <?php $title = get_post_meta(get_the_ID(), 'sum_e_caption', true); ?>
                            <?php if (!$pic == ""){ echo '<img src="'. $pic. '" class="img-responsive archive-largo-fotos" itemprop="image thumbnailURL" alt="Sumarium - '.get_the_title() .'"/>'; ?>
                            <?php } else { echo '<img src="' . get_bloginfo( 'template_url' ) . '/images/no_pic.gif" class="archive-no-image" itemprop="image thumbnailURL" alt="Sumarium - '.get_the_title() .'"/>';
                                         } ?>
                            <div class="archive-fotos-title"><header><a href="<?php the_permalink(); ?>" itemprop="URL"><h1 itemprop="about"><?php the_title() ?></h1></a></header></div>
                        </div>
                    </a>

                </div>
            </article>
            <div class="col-md-1"><a id="<?php echo $siguiente; ?>" href="#" onclick="next(this.id);"><i class="fa fa-angle-right fa-5x"></i></a></div>
            <div class="clearfix"></div>
            <?php wp_reset_postdata(); ?>

        </div>
    </div>
</div>
<?php get_footer(); ?>
