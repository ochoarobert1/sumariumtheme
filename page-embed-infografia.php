<?php $post_id = $_GET['t']; ?>
<?php global $wpdb; ?>
<?php $post = get_post($post_id); ?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title><?php echo $post->post_title; ?></title>
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/main-core-style.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/special-functions.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/sumarium-style.css" type="text/css" />
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/sumarium-mediaqueries.css" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $post->post_content; ?>
                </div>
            </div>
        </div>
        <style type="text/css">
            <?php echo get_post_meta($post_id, 'sum_customcss', true); ?>
        </style>
        <script type="text/javascript">
            <?php echo get_post_meta(get_the_ID(), 'sum_customjs', true); ?>
        </script>
    </body>
</html>
