<?php get_header(); ?>
<?php
/*

  Contenidos
  zonas = nt24_pnt24_zona
  - id_zona
  - activo
  - posicion
  - separador

  lugares = nt24_pnt24_lugar
  - id_lugar
  - id_zona
  - tipo
  - plantilla
  - fondo_url
  - activo

  items = nt24_pnt24_item
  - id_item
  - id_lugar
  - item
  - id_pub
 */
?>

<?php
//QUERY PARA TRAER LAS ZONAS ACTIVAS EN ORDEN DE POSICION
global $wpdb;
$zonas = $wpdb->get_results(
    '
            SELECT * FROM ' . $wpdb->prefix . 'pnt24_zona
            WHERE activo = 1
            ORDER BY posicion
        '
);


$nzonas = count($zonas);
$clase = "";
$z = 1; //contador de zonas
$d = 1; // contador para cintillo
$u = 1; // contador de urgente
foreach ($zonas as $zona) {
    if ($z > 1){ $clase = "lazyload"; }
?>
<div id="zona<?php echo $z; ?>" class="zona <?php echo $clase; ?>">
    <!-- ZONAS -->

    <?php
    //HACER EL QUERY PARA LOS DESTACADOS
    global $wpdb;
    $destacados = $wpdb->get_results(
        '
                SELECT * FROM ' . $wpdb->prefix . 'pnt24_lugar
                WHERE id_zona = ' . $zona->id_zona . '
                AND TIPO = 1
                AND activo = 1
                ORDER BY id_zona ASC
                LIMIT 1
            '
    );

    foreach ($destacados as $destacado) {

        //hacer la consulta para los posts
        global $wpdb;
        $item = $wpdb->get_results(
            '
                    SELECT * FROM ' . $wpdb->prefix . 'pnt24_item
                    WHERE id_lugar = ' . $destacado->id_lugar . '
                '
        );


        if (!empty($destacado->fondo_url)) {
            $fondo = "width:100%;background-size:cover;background-image: url(" . $destacado->fondo_url . ");";
        } else {
            $fondo = '';
        }
        $l = 1; //contador de lugares
        // MOSTRARNDO DESTACADOS DE LA ZONA
    ?>
    <?php
        $lugar = 'templates/page-' . $destacados[0]->plantilla . '-noticias.php';
        include(locate_template($lugar));
        if ($d == 1){
            include(locate_template('templates/ad-thin-fullwidth.php'));
            $d++;
        }
    ?>
    <?php
    } //FIN DESTACADO
    ?>
    <div class="clearfix"></div>

    <?php
    //HACER EL QUERY PARA LOS BLOQUES
    global $wpdb;
    $bloques = $wpdb->get_results(
        '
                SELECT * FROM ' . $wpdb->prefix . 'pnt24_lugar
                WHERE id_zona = ' . $zona->id_zona . '
                AND TIPO = 2
                AND activo = 1
                ORDER BY id_zona
                LIMIT 1
            '
    );
    // MOSTRARNDO BLOQUE DE LA ZONA
    foreach ($bloques as $bloque) {
        //echo var_dump($bloque);
        #  Esqueleto para la parte inferior de la seccion completa
        #  Este esqueleto va luego del frame de destacados en cada seccion
        /* Tiene 3 columnas, de izquierda a derecha:
              Columna 1: Noticias en formato de 5 columnnas
              Columna 2: Noticass en formato de 3 columnas
              columna 3: Sidebar para publicidad */
    ?>
    <section id="skeleton-loop">
        <div class="skeleton-loop">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12 no-paddingr no-paddingl padding-xs">

                        <?php global $wpdb;
        $titulo = $wpdb->get_results(" SELECT * FROM " . $wpdb->prefix . "pnt24_lugar WHERE id_zona = " . $zona->id_zona . " AND tipo = 2 AND activo = 1");
        if (!empty($titulo[0]->fondo_url) || !empty($titulo[0]->texto)) {
                        ?>
                        <div class="col-md-12 col-xs-12 col-sm-12 no-paddingr no-paddingl block-front-title">
                            <?php if (!empty($titulo[0]->texto)) {?>
                            <div class="col-md-3 col-xs-2 col-sm-3 no-paddingl no-paddingr separador-outside"></div>
                            <div class="col-md-6 col-xs-8 col-sm-6 separador-inside no-paddingl no-paddingr" style="background:<?php echo $titulo[0]->fond_color; ?>;">
                                <div class="col-md-12 separador-text" style="background:<?php echo $titulo[0]->fond_color; ?>; border: 1px solid <?php echo $titulo[0]->text_color;?>; color: <?php echo $titulo[0]->text_color;?>;">
                                    <?php echo $titulo[0]->texto;?>
                                </div>
                            </div>
                            <div class="col-md-3 col-xs-2 col-sm-3 no-paddingl no-paddingr separador-outside"></div>
                            <?php } else { ?>
                            <?php /*if (!empty($titulo[0]->fondo_url)) {*/ ?>
                            <img src="<?php echo $titulo[0]->fondo_url; ?>" alt="<?php echo $titulo[0]->texto; ?>" class="img-responsive" />
                            <?php } ?>
                        </div>
                        <div class="clearfix"></div>

                        <?php } ?>

                        <?php
        $elements = $wpdb->get_results(" SELECT * FROM ".$wpdb->prefix."pnt24_pending left join ".$wpdb->prefix."pnt24_into_pending using(id_pending) left join ".$wpdb->prefix."terms using(term_id) where id_lugar = ".$bloque->id_lugar);
        if ($elements[0]->active == '1'){
                        ?>
                        <div class="col-md-12  no-paddingr no-paddingl block-front-title block-tags-container">
                            <ul class="block-tags-content">
                                <?php foreach($elements as $term){ ?>
                                <li class="block-item"><a href="<?php echo home_url('/');?>tag/<?php echo $term->slug; ?>"><div class="block-text"><?php echo $term->name;?></div></a></li>
                                <?php } ?>
                            </ul>
                        </div>

                        <?php } ?>

                        <div class="col-md-7 col-sm-7 col-xs-7 no-paddingl">
                            <?php include(locate_template('templates/five-cols-front-item.php')); ?>
                        </div>
                        <div class="three-cols-main-container col-md-5 col-sm-5 col-xs-5">
                            <?php include(locate_template('templates/three-cols-front-item.php')); ?>
                        </div>

                    </div>

                    <div class="col-md-4 col-sm-12 col-xs-12 padding-xs no-paddingr no-paddingl">
                        <div class="sidebar-section-loop">
                            <?php
        include(locate_template('templates/sidebar-front-page.php'));
        //get_template_part('templates/sidebar-front-page');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
        //get_template_part('templates/skeleton-section-loop');
    }//FIN BLOQUE
    ?>
    <?php

    if ($zona->pics == '1') {
        get_template_part('templates/featured-pics');
    }

    if ($zona->voces == '1') {
        get_template_part('templates/featured-opinions');
    }
    ?>
    <?php
    //cargar el separador
    if ($zona->separador > 0) {
    ?>
    <div class="container zoneads">
        <div class="row">
            <?php the_ad_group($zona->separador); ?>
        </div>
    </div>
    <?php }
    ?>
</div>
<?php
    $z++;
}; //FIN ZONAS
?>
<?php get_footer(); ?>
