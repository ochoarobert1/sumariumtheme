<?php get_header(); ?>
<div class="container-fluid">
    <div class="row">
        <div id="slide-2" class="col-md-12 sumarium-section1 no-paddingl no-paddingr" data-0-top="background-position[quadratic]: 0px 50%;" data-50-top="background-position[quadratic]: 0px 70%" data-500-top="background-position[quadratic]: 0px 80%" data-anchor-target="#slide-2">
            <div class="sumarium-section1-logo col-md-4 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 col-md-offset-4">
                <img id="logo-1" src="<?php bloginfo('template_url')?>/images/logo-big.png" alt="logo" class="img-responsive" data--200-top-top="opacity: 0" data-306-top="opacity: 1" data-anchor-target="#logo-1"/>
            </div>
            <div class="sumarium-section1-mask"></div>
            <h1 id="text" class="text-center slogan">"No se puede ser imparcial entre la verdad y la mentira"</h1>
        </div>

        <div class="col-md-12 sumarium-section2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 id="periodismo" class="text-center">El periodismo que hacemos</h1>
                        <p><i>Sumarium.com</i> nace porque sentimos que hace falta una <strong>cobertura completa de los hechos que ocurren en nuestra región orientada a los lectores globales</strong>. Porque al final del día si un lector está en Venezuela o Colombia, lo que sucede en Argentina o España le importa y es nuestra labor no solo informar, sino explicar por qué nos interesa y cómo estamos conectados más allá del suelo que compartimos en esta parte del mundo.</p>
                        <p><strong>¿Qué pasa en la región? ¿Cómo nos afecta? ¿Por qué nos importa?</strong> Son preguntas en las que cada día el equipo de Sumarium.com trabaja para dar respuestas, esto junto al aporte de las <a href="<?php echo home_url('/categoria/opinion/')?>">Voces Sumarium</a>, compuestas por los analistas más acertados que nos ayudarán a desenmarañar lo que sucede, así como el trabajo de nuestros colaboradores y el aporte que tiene usted, amigo lector, para ayudarnos a hacer un mejor trabajo cada día, exigiéndonos, iluminándonos y participando.</p>
                        <p>En <i>Sumarium.com</i> ejercemos un periodismo sin cortapisa, sin los límites y la presión que el poder y los intereses económicos y políticos muchas veces pretenden poner sobre los medios de comunicación y que derivan en la adulteración -sea por omisión, exaltación o incluso fabricación- de la información que le llega al usuario con miras a manipular la opinión pública.</p>
                        <p>"Sólo mediante la libre expresión y circulación de ideas, la búsqueda y difusión de informaciones, la posibilidad de indagar y cuestionar, de exponer y reaccionar, de coincidir y discrepar, de dialogar y confrontar, de publicar y transmitir, es posible mantener una sociedad libre. Sólo mediante la práctica de estos principios será posible garantizar a los ciudadanos y grupos su derecho a recibir información imparcial y oportuna. <strong>Sólo mediante la discusión abierta y la información sin barreras será posible buscar respuestas a los grandes problemas colectivos, crear consensos</strong>, permitir que el desarrollo beneficie a todos los sectores, ejercer la justicia social y avanzar en el logro de la equidad", reza el preámbulo de la <a href="http://www.declaraciondechapultepec.org/declaracion_chapultepec.htm" target="_blank">Declaración de Chapultepec</a>, a la cual nos apegamos.</p>
                        <p>En un panorama informativo en el que el peso del <i>breaking news</i> es cada vez mayor, con un seguimiento casi frenético de la noticia de manera fragmentada e incluso desordenada, vemos que en muchos casos el espacio para el análisis reflexivo de los acontecimientos y sus consecuencias para la gente está cada vez más disminuido. Es precisamente <strong>ese delicado balance entre la noticia en caliente, el análisis y las herramientas de comprensión el norte de <i>Sumarium.com</i></strong>.</p>
                        <p>Acompáñenos en esta travesía.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 sumarium-section3">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 id="principios" class="text-center">Principios que no quebramos</h1>
                        <p><strong>"No se puede ser imparcial entre la verdad y la mentira"</strong> es nuestro principio fundamental y la meta que comparte el equipo de <i>Sumarium.com</i> es la presentación de noticias e información de calidad, sin “temor ni favor”, con un tratamiento justo, abierto y transparente.</p>
                        <p>Partiendo de la amplia experiencia en medios de comunicación acumulada por los integrantes de este proyecto, y entendiendo que la libertad de expresión es fundamental para la consolidación y desarrollo de la democracia de un país, <i>Sumarium.com</i> expone a los lectores las normas bajo las que actuamos y que servirán para contribuir con la autorregulación profesional y a la consolidación periodística.</p>
                        <p><i>Sumarium.com</i> se planta con firmeza desde su inicio para decir que <strong>no permite presión alguna de los poderes políticos y económicos</strong> que en los últimos años han aumentado buscando cercenar medios de comunicación, sin entender que en los tiempos que corren la información se abre paso y fluye.</p>
                        <p><i>Sumarium.com</i> <strong>no publica notas de prensa de ninguna índole</strong>, en todo caso podemos tomar datos relevantes de estos comunicados oficiales y nos esforzaremos por buscar las respectivas fuentes y antecedentes que nos permitan presentar la información lo más completa y clara posible permitiendo que el lector forme su propio criterio en función de la realidad que entienda en referido tema.</p>
                        <p><i>Sumarium.com</i> <strong>no permitirá la interferencia o presiones directas o indirectas sobre cualquier material escrito, fotográfico o audiovisual difundido</strong>, tampoco permitirá la imposición arbitraria o presiones que obstaculicen el libre flujo informativo ya que estaríamos condenados al fracaso rotundo y al irrespeto hacia nuestros lectores.</p>
                        <p>En países democráticos la necesidad de unidad y templanza debe ser permanente, por ello <i>Sumarium.com</i> dispone de la <strong><a href="mailto:LectoresSumarium@gmail.com">Unidad de Defensa del Lector</a></strong>, la misma permitirá un espacio para la réplica de este, en caso de sentirse agredido o vulnerado por cualquier material publicado en estas páginas.</p>

                        <p><i>Sumarium.com</i> <strong>no cambia o elimina textos, fotografías o publicaciones por complacer los gustos o intereses</strong> de cualquier individuo que así lo pretenda, en todo caso está a su disposición el correo <a href="mailto:LectoresSumarium@gmail.com">LectoresSumarium@gmail.com</a> para realizar alguna solicitud de réplica y exponer su caso.</p>
                        <p>Ningún dirigente, líder religioso, partido político o funcionario de gobierno podrá silenciar la labor que desde <i>Sumarium.com</i> se realiza, cualquier intento -además de violar las reglas periodísticas- <strong>será señalado por abuso al derecho a la libertad de prensa y de información.</strong></p>
                        <p><i>Sumarium.com</i> deja claro que <strong>no se convertirá en un espacio para el tráfico de declaraciones cargadas de violencia verbal y agresividad</strong>, tampoco será una bandeja al servicio de cualquier grupo económico, político o social. Establecer claramente estos mecanismos evitará la práctica de abusos de líderes cada vez más extremistas y radicalizados.</p>
                        <p>La independencia de este medio y la no manipulación de las noticias será la garantía para nuestro público, quienes en  última instancia son el pasaporte del trabajo periodístico que aquí se realiza.</p>
                        <p>La defensa de la libertad de expresión pasa por el establecimiento de mecanismos de transparencia en el ejercicio de esta profesión, a fin de no arruinar el único patrimonio de nuestro oficio: la credibilidad. Entre esos mecanismos figura por propios méritos esta normativa, que servirá para defender a los lectores del sensacionalismo, el amarillismo y el corporativismo de los profesionales. Porque a veces ocurre que <strong>en la mención abusiva de la libertad de información y de expresión se escudan sus enemigos para negar las críticas legítimas</strong> y la labor de control del poder, incluido el de los propios periodistas.</p>
                        <p>Convertir los medios de comunicación en armas del tráfico de influencias al servicio de intereses que no se declaran es una práctica de abuso que crece a la sombra de la libertad.</p>
                        <p><strong>Si es noticia, estará en <i>Sumarium.com.</i> Si no lo es, no perderemos nuestro tiempo</strong>, ni el suyo estimado lector.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 sumarium-section4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 info-editor">
                        <h1 id="somos-sumarium" class="text-center">Somos Sumarium</h1>
                        <div class="col-md-3">
                            <img src="<?php bloginfo('template_url')?>/images/editores/ambar.jpg" alt="editor" class="img-responsive img-editor" />
                        </div>
                        <div class="col-md-9">
                            <h2>Ambar Rengel</h2>
                            <h4><a href="https://www.twitter.com/AmbarRengel" target="_blank">@AmbarRengel</a> | <a href="mailto:AmbarMarinarl@gmail.com">AmbarMRengel@gmail.com</a> </h4>
                            <p>Comunicadora por vocación, más que por profesión. Trabajó en 2007 en el semanario Urbe. En 2008 ingresó como redactora al equipo de Noticias24, pasando a ser en 2010 directora adjunta del portal hasta el 2013. Fue directora del Bloque de Prensa Digital de Venezuela en 2012. Considera que la capacidad de contar bien las historias es fundamental para cualquier comunicador, ya que tiene en sus manos el poder de cambiar vidas.</p>
                            <h3>"El periodismo debe ser sensible, curioso, implacable con el poder, autocrítico y analítico. Al final del día, el interés que debe prevalecer es el de contribuir con la sociedad para ayudar a motorizar los cambios necesarios"</h3>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 info-editor">
                        <div class="col-md-3">
                            <img src="<?php bloginfo('template_url')?>/images/editores/leonor.jpg" alt="editor" class="img-responsive img-editor" />
                        </div>
                        <div class="col-md-9">
                            <h2>Leonor Roquett</h2>
                            <h4><a href="https://www.twitter.com/Leonor_Roquett" target="_blank">@Leonor_Roquett</a> | <a href="mailto:Leonor.Roquett@gmail.com">Leonor.Roquett@gmail.com</a> </h4>
                            <p>Periodista egresada de la Universidad Central de Venezuela. Parte de la delegación de la UCV que obtuvo el primer lugar por dos años consecutivos en el modelo de las Naciones Unidas en Nueva York. Entró a Noticias24 en 2012. Condujo un programa de entrevistas junto al periodista Mario Villegas. Dirigió la televisión de Noticias24 desde 2014. Actualmente cursa la especialización de Periodismo de Investigación en la Ucab. Cree que lo excitante del periodismo radica en la incapacidad de no poder estar estático: de la TV y la radio a la redacción, de una historia a otra, siempre con el mismo empuje.</p>
                            <h3>"Cuando te das cuentas que convertiste tu vocación por el periodismo en una pasión, entiendes que vas por el camino correcto"</h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 info-editor">

                        <div class="col-md-3">
                            <img src="<?php bloginfo('template_url')?>/images/editores/any.jpg" alt="editor" class="img-responsive img-editor" />
                        </div>
                        <div class="col-md-9">
                            <h2>Aniger Esteves</h2>
                            <h4><a href="https://www.twitter.com/AnyEsteves" target="_blank">@AnyEsteves</a> | <a href="mailto:Any.esteves@gmail.com">Any.Esteves@gmail.com</a></h4>
                            <p>Comunicadora social egresada de la Universidad Santa María en la mención de Periodismo Impreso. Dedicada a la profesión desde hace ocho años, ha complementado su formación en medios digitales y radiales, dando cobertura a las fuentes de política, economía, finanzas y petróleo. Su última experiencia: redactora del equipo de Investigación de Noticias24, reportera y ancla de Noticias24 TV. Su mayor pasión: la investigación.</p>
                            <h3>"La sensibilidad ante lo propio y lo ajeno, el compromiso con la verdad y el respeto hacia el ser humano son principios irrenunciables en el ejercicio del periodismo".</h3>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 info-editor">
                        <div class="col-md-3">
                            <img src="<?php bloginfo('template_url')?>/images/editores/margiory.jpg" alt="editor" class="img-responsive img-editor" />
                        </div>
                        <div class="col-md-9">
                            <h2>Margiory Fiaschi</h2>
                            <h4><a href="https://www.twitter.com/MaggiFiaschi" target="_blank">@MaggiFiaschi</a> | <a href="mailto:MaggiFiaschi@gmail.com">MaggiFiaschi@gmail.com</a></h4>
                            <p>Periodista venezolana con más de 10 años de experiencia. Sus inicios surgieron en la tv regional abriéndole camino hasta estar al frente de las corresponsalías de Aragua y Carabobo para RCTV y Venevisión. Fue directora de Noticias24Carabobo hasta octubre de 2014. Su mayor satisfacción: servir de voz para quienes son menos escuchados, recordar con su pluma que a Venezuela la conforma gente común que vale la pena.</p>
                            <h3>"Más que una profesión tengo la responsabilidad contribuir con la sociedad sembrando  semillas de tolerancia, de respeto, de sensatez y apostando por el futuro"</h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 info-editor">
                        <div class="col-md-3">
                            <img src="<?php bloginfo('template_url')?>/images/editores/ronel.jpg" alt="editor" class="img-responsive img-editor" />
                        </div>
                        <div class="col-md-9">
                            <h2>Ronel González</h2>
                            <h4><a href="https://www.twitter.com/RonCcs" target="_blank">@RonCcs</a> | <a href="mailto:RonGonzalezc@gmail.com ">RonGonzalezc@gmail.com </a></h4>
                            <p>Es graduado de la Universidad Bicentenaria de Aragua, en Venezuela. Trabajó en varios medios regionales antes de desempeñar diversos puestos en Noticias24. Cree que el periodismo necesita contar más y opinar menos.</p>
                            <h3>"Si el periodismo no muestra y analiza las dos caras de un hecho corre el riesgo de tomar parte. Perderá credibilidad y sentido".</h3>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 info-editor">
                        <div class="col-md-3">
                            <img src="<?php bloginfo('template_url')?>/images/editores/andres.jpg" alt="editor" class="img-responsive img-editor" />
                        </div>
                        <div class="col-md-9">
                            <h2>Andrés Eduardo Herrera</h2>
                            <h4><a href="https://www.twitter.com/AndrEduardo" target="_blank">@AndrEduardo</a> | <a href="mailto:AHerreraSumarium@gmail.com">AHerreraSumarium@gmail.com</a></h4>
                            <p>Egresó como Licenciado en Comunicación Social, mención Periodismo Impreso, de la Universidad Rafael Belloso Chacín. Inició su ejercicio en el área como asistente de producción, redactor creativo y locutor, para luego enfocarse en análisis periodísticos y entrevistas sobre política, geopolítica y economía. Estima que la información oportuna y masificada permite a las sociedades sortear obstáculos y planificar en función de la realidad.</p>
                            <h3>"Los pies deben mantenerse sobre la Tierra; la visión, lo más allá posible"</h3>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 info-editor">
                        <div class="col-md-3">
                            <img src="<?php bloginfo('template_url')?>/images/editores/mariely.jpg" alt="editor" class="img-responsive img-editor" />
                        </div>
                        <div class="col-md-9">
                            <h2>Mariely Marquez</h2>
                            <h4><a href="https://www.twitter.com/PeriodistaMarie" target="_blank">@PeriodistaMarie</a> | <a href="mailto:MarielyMarquezc@gmail.com">MarielyMarquezc@gmail.com</a></h4>
                            <p>Periodista zuliana con 7 años de experiencia en el medio. Comenzó su carrera en periódicos impresos hasta llegar al periodismo digital en Noticias24, donde trabajó por más de 4 años cubriendo diferentes fuentes de información. Para Mariely el periodismo es una caja de sorpresas, que depende del acontecer diario.</p>
                            <h3>"Para mi ser periodista es estar llena de adrenalina para transmitir lo que es noticia del día a día. No estoy  acuerdo con la injusticia, pero respeto las diferentes opiniones"</h3>
                        </div>

                        <div class="clearfix"></div>
                    </div>




                </div>
            </div>
        </div>
        <div class="col-md-12 sumarium-section5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 id="habla-con-nosotros" class="text-center">Habla con nosotros</h1>
                        <p>Como hemos venido diciendo, <strong>la transparencia es para nosotros fundamental, es por ello que somos abiertos a la crítica, las sugerencias y comentarios de nuestros lectores</strong> y también de las personas que son protagonistas de las noticias. En <i>Sumarium.com</i> estamos abiertos, además, a la rectificación y el reconocimiento de los errores cuando los cometemos.</p>
                        <p>Es por ello que colocamos cara y nombre a los periodistas que hacemos <i>Sumarium.com</i> y ponemos a la vista del público las vías de comunicación con nuestro equipo.</p>
                        <p>Con esto en mente, dejamos también a su disposición el correo <a href="mailto:LectoresSumarium@gmail.com">LectoresSumarium@gmail.com</a> para que nos escriba, sin límite alguno, de lo que desee referido a nuestra cobertura periodística. Puede además <strong>decirnos problemas puntuales de su comunidad, expresar su preocupación por las coyunturas que afectan nuestra región</strong> y sugerirnos temas para su abordaje periodístico.</p>
                        <p>A través de <a href="mailto:LectoresSumarium@gmail.com">LectoresSumarium@gmail.com</a> también activamos los mecanismos de la Defensoría del Lector, con el que serán llevadas a las reuniones editoriales las observaciones de quienes nos leen.</p>
                        <p><strong>Estas misivas podrían ser publicadas en nuestro portal</strong> y representan una vía de participación para usted, amigo lector, pues <i>Sumarium.com</i> no es una máquina de montar noticias, es un órgano vivo, como las noticias y el público que las lee.</p>
                        <h3><p>¡Bienvenidos!</p></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<?php get_footer('sumarium'); ?>
