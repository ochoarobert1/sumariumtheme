function show_taged(x,y){
    // ruta (variable de template url)
    var item = x;
    var ant = y;
    $.ajax({
        type : 'POST',
        url : ruta + '/templates/show-tag-function.php',
        data: {
            id: item
        },
        beforeSend: function(){
            $('#tag_frame').html(
                '<div class="col-md-12 text-center">'
                +'<p>Cargando<p>'
                +'<img src="' + ruta + '/images/AjaxTagLoader.gif">'
                +'</div>'
            );
        },
        success: function(resp){
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#tag_frame').html(resp);
            $(".tag-news-title2 > *").removeClass('activo');
            $('#title-' + x).addClass('activo');
            setTimeout(function() {
                tagheight = $('#tag_frame').height() + 300;
                $('#tags-bar').height(tagheight);
            }, 2000);
        }
    })
}
