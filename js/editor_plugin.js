(function() {
	tinymce.PluginManager.add('my_mce_button', function( editor, url ) {
		editor.addButton('my_mce_button', {
			text: 'Marco para Imagen',
			icon: false,
			onclick: function() {
				editor.insertContent(
                            '[image-frame]' +
                            editor.selection
                            .getContent() +
                            '[/image-frame]'
                        );
			}
		});
	});
})();