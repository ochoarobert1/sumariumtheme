<div class="front-ads-container">

    <?php
        $sidebar = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "pnt24_sidebars left join " . $wpdb->prefix . "pnt24_into_sidebar using(id_sidebar) where active <> 0 AND id_lugar = " . $bloque->id_lugar . " ORDER BY posicion ASC" );
    ?>

    <?php foreach ($sidebar as $side) { ?>
        <div class="col-md-12 ads-single">
            <div class="sidebar-front-page-news">
            <?php
            //variable a utilizar para delimitar la cantidad de noticias a montrar por cada sidebar
            $cantidad = $side->cantidad;
            //variable con el nombre del tag
            $tag = $side->tag;
            //variable con la url para la imagen de fondo de editor pick
            $url_fondo = $side->url_fondo;
            //variable para la busqueda del tag
            //$slug = $side->slug;
            ?>

              <?php if ($side->tipo == 1) { ?>
                  <div class="col-md-12 ads-single visible-md visible-lg">
                    <?php include(locate_template('templates/sidebar-most-recent.php'));?>
                  </div>
              <?php } ?>

              <?php if ($side->tipo == 2) { ?>
                  <div class="col-md-12 ads-single visible-md visible-lg">
                        <?php include(locate_template('templates/sidebar-most-view.php'));?>
                  </div>
              <?php } ?>

              <?php if ($side->tipo == 3) { ?>
                  <div class="col-md-12 col-xs-6 col-sm-6 ads-single">
                    <?php include(locate_template('templates/sidebar-ministories.php'));?>
                  </div>
              <?php } ?>

              <?php if ($side->tipo == 4) { ?>
                  <div class="col-md-12 ads-single visible-md visible-lg">
                    <?php include(locate_template('templates/sidebar-editorspick.php'));?>
                  </div>
              <?php } ?>

            </div>
        </div>
  <?php } ?>

</div>

