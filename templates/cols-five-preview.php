<div class="five-cols-front-list">
    <div class="row">
        <?php for($i = 1; $i <= $can_blo1; $i++){
    //obtener datos del post
    $slug = "";
    $estilo = "";
    $post = get_post($itemcol1[$i]->item);
    $titulo = $post->post_title;
    $autor = $post->post_author;
    $user_info = get_userdata($autor);
    $noimg = "";
    $terms = get_the_terms($itemcol1[$i]->item, 'content_taxonomy');
    if ( $terms != null ){ foreach ( $terms as $term ) { $slug = $term->slug; } }
    switch ($slug) {
        case 'analisis': $estilo = "pencil"; break;
        case 'opinion': $estilo = "pencil"; break;
        case 'fotos': $estilo = "camera"; break;
        case 'infografia': $estilo = "area-chart"; break;
        case 'video': $estilo = "play"; break;
        default: $estilo = ""; break;
    }
        ?>
        <div class="col-md-12 five-cols-front-item">
            <article>
                <a class="hover-link" href="<?php the_permalink(); ?>">
                    <?php if ($estilo != ""){ echo '<div class="img-content-tag-small-five-cols"><i class="fa fa-'. $estilo .'"></i></div>'; } ?>
                    <?php if (!empty($itemcol1[$i]->thumbnail_url)) { ?>
                    <div class="five-cols-img-wrapper">
                        <img src="<?php echo $itemcol1[$i]->thumbnail_url; ?>" alt="Sumarium" />
                    </div>
                    <?php } else { $noimg = "front-link-noimg"; } ?>
                    <div class="five-cols-title <?php echo 'five-cols-'.$noimg; ?>">
                        <header>
                            <a class="front-link" href="<?php the_permalink(); ?>">
                                <h1 class="five-cols-item-title <?php echo $noimg; ?>"><?php echo $titulo; ?></h1>
                                <br />
                                <h3 class="five-cols-item-author <?php echo $noimg; ?>">Por: <?php echo $user_info->display_name; ?></h3>
                            </a>
                        </header>
                    </div>
                </a>
            </article>
        </div>
        <?php } ?>
    </div>
</div>
