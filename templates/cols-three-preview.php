<div class="three-cols-front-list">
    <div class="row">
        <?php 
            $pivote = 0;
            for ($i = 1; $i <= $can_blo2; $i++) {
            $nota = $itemcol2[$i]->item;
                if($nota != $pivote){
                    $pivote = $itemcol2[$i]->item;
                    $post = get_post($itemcol2[$i]->item);
                    $titulo = $post->post_title;
                    $autor = $post->post_author;
                    $user_info = get_userdata($autor);
                    $noimg = "";
        ?>
        <div class="col-md-12 no-paddingl no-paddingr three-cols-front-item" style="background: <?php echo $itemcol2[$i]->fond_color; ?>">
            <article>
                <a class="hover-link" href="<?php the_permalink(); ?>">
                    <?php if (!empty($itemcol2[$i]->thumbnail_url)) { ?>
                    <div class="three-cols-img-wrapper">
                        <img src="<?php echo $itemcol2[$i]->thumbnail_url; ?>" alt="Sumarium" />
                        <div class="three-cols-img-wrapper-mask"></div>
                    </div>
                    <?php } ?>
                    <div class="three-cols-title <?php echo 'three-cols-'.$noimg; ?>">
                        <header>
                            <a class="front-link" href="<?php the_permalink(); ?>">
                                <h1 style="color: <?php echo $itemcol2[$i]->text_color; ?>" class="three-cols-item-title <?php echo $noimg; ?>"><?php echo $titulo; ?></h1>
                            </a>
                        </header>
                    </div>
                </a>
            </article>
        </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
