<?php

#  Esqueleto para la parte inferior de la seccion completa
#  Este esqueleto va luego del frame de destacados en cada seccion
/* Tiene 3 columnas, de izquierda a derecha:
     Columna 1: Noticias en formato de 5 columnnas
     Columna 2: Noticass en formato de 3 columnas
     columna 3: Sidebar para publicidad */
?>
<section id="skeleton-loop">
    <div class="skeleton-loop">
        <div class="container">
            <div class="row">
                <div class="col-md-5 nosidepadding">
                    <div class="five-cols-front-list">
                        <?php 
                            get_template_part('templates/five-cols-ads-frame');
                        ?>
                        <?php
                            wp_reset_query();
                            query_posts( 'posts_per_page=6' );
                            if(have_posts()){
                                $i = 1;
                                while(have_posts()){ the_post();
                                    get_template_part('templates/five-cols-front-item');
                                    $ids[] .= get_the_ID();
                                    if($i % 2 == 0){ ?>
                                        <?php get_template_part('templates/five-cols-ads-frame'); ?>
                                    <?php }
                                    $i++;
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="three-cols-front-list">
                        <?php
                            query_posts(array('posts_per_page' => 10, 'post__not_in' => $ids));
                            //query_posts( $newquery );
                            if(have_posts()){
                                while(have_posts()){ the_post();
                                    get_template_part('templates/three-cols-front-item');
                                }
                            }
                        ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="sidebar-section-loop">
                        <?php get_template_part('templates/sidebar-front-page'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>