<div class="col-md-3 single-ads-container">
    <div class="col-md-12 ads-single">
        <img src="<?php bloginfo('template_url');?>/images/addthis.png" alt="ads1" />
    </div>
    <?php 
        wp_reset_postdata();
        /* THE QUERY */
        $cat_id = get_cat_ID($cat);
        $args = array( 'posts_per_page' => 4, 'category' => $cat_id );
        $query_posts = get_posts($args);
    ?>
    <div class="col-md-12 ads-single">
       <div class="sidebar-front-page-news">
            <div class="sidebar-front-news-title sidebar-front-news-<?php echo normaliza (strtolower($cat)); ?>">
                <?php echo strtoupper($cat);?> > LOS MAS COMENTADOS
            </div>
            <?php 
                $i=1;
                foreach ($query_posts as $post) : setup_postdata( $post );
                    echo '<div class="sidebar-front-news-content"><article>';
                    if ( has_post_thumbnail() ) {
                        the_post_thumbnail();
                    } else {
                       echo '<img src="' . get_bloginfo( 'template_url' ) . '/images/no_pic.gif" />';
                    }
                    echo '<a href="'.get_permalink().'">';
                    echo '<div class="sidebar-front-news-single-title">';
                    echo '<h1>'.get_the_title().'</h1>';
                    echo '</div>';
                    echo '</a>';
                    echo '</article></div>';
                endforeach; 
                wp_reset_postdata();?>              
       </div>
    </div>
</div>