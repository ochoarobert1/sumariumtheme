<div class="container">
    <div class="row">
        <div class="col-md-12 no-paddingl no-paddingr">
            <section id="featured-opinions">
                <div class="featured-opinions-title col-md-12  no-paddingl no-paddingr">
                    <h3>Opinión</h3>
                </div>
                <div class="featured-opinions-title2 col-md-12  no-paddingl no-paddingr">Voces Sumarium</div>
                <div id="owl-opinions"  class="featured-opinions-content col-md-12 no-paddingl owl-carousel owl-theme">
                    <?php $y = 1; $padre = 0; query_posts( array ('category_name' => 'opinion', 'posts_per_page' => 8 ) ); ?>
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    <div class="item">
                        <div class="media">
                            <div class="media-left">
                                <a href="<?php the_permalink(); ?>">
                                    <?php
$terms = get_the_terms( $post->ID , 'content_taxonomy' );
foreach ($terms as $term) {
    if (($term->parent == 60) && ($padre == 0)) {
        $termname = $term->name;
        $termslug = $term->slug;
        $padre = 1;
    }
}
if ($padre == 1){
    echo '<img src="'. get_bloginfo('template_url').'/images/logos/' . $termslug . '.jpg" alt="' . $termslug . '" class="media-img-front" />';
} else {
    echo get_avatar( get_the_author_meta('ID'), "120");
}

                                    ?>
                                    <!--                                    <img class="media-object" src="https://theinformedconservative.files.wordpress.com/2014/05/24603_383464361748_4038418_n.jpg" alt="...">-->
                                </a>
                            </div>
                            <div class="media-body">
                                <a href="<?php the_permalink(); ?>">
                                    <h5 class="media-heading"><?php the_title(); ?></h5>
                                    <?php if ($padre == 1){
                                        echo '';
                                    } else { ?>
                                    Por: <strong><?php the_author(); ?></strong>
                                    <?php } ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php $padre = 0; $y++; endwhile; ?>
                    <div class="clearfix"></div>
                    <?php wp_reset_postdata(); ?>
                    <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                    <?php endif; ?>
                </div>
            </section>
        </div>
    </div>
</div>
