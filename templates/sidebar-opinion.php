
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fechats = explode('-',$fecha);
    $today['year'] = $fechats[0];
    $today['mon'] = $fechats[1];
    $today['mday'] = $fechats[2];
} else {
    $today = getdate();
}
$args=array(
    'date_query' => array(
        array(
            'year'  => $today['year'],
            'month' => $today['mon'],
            'day'   => $today['mday'],
        ),
    ),
    'content_taxonomy' => 'voces-sumarium',
    'posts_per_page' => 3
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) { ?>
<div class="sidebar-opinion-container visible-md visible-lg">
    <div class="sidebar-opinion-bigtitle">
        <h3>Voces Sumarium</h3>
    </div>
    <div class="col-md-12 sidebar-opinion-content">
        <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
        <div class="media">
            <div class="media-left">
                <a href="#">
                    <?php echo get_avatar( get_the_author_meta('ID'), "120"); ?>
                </a>
            </div>
            <div class="media-body">
                <a href="<?php the_permalink(); ?>">
                    <h4 class="media-heading"><?php the_title(); ?></h4>
                    <h5>Por: <strong><?php the_author(); ?></strong></h5>
                </a>
            </div>
        </div>
        <?php endwhile; ?>
        <div class="clearfix"></div>
        <?php wp_reset_postdata(); ?>
    </div>
</div>
<?php } ?>
