<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fechats = explode('-',$fecha);
    $today['year'] = $fechats[0];
    $today['mon'] = $fechats[1];
    $today['mday'] = $fechats[2];
} else {
    $today = getdate();
}
$args=array(
    'date_query' => array(
        array(
            'year'  => $today['year'],
            'month' => $today['mon'],
            'day'   => $today['mday'],
        ),
    ),
    'content_taxonomy' => 'editorial-ext',
    'posts_per_page' => 3
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) { ?>
<div class="sidebar-opinion-container visible-md visible-lg">
    <div class="sidebar-opinion-bigtitle">
        <h3>Las Editoriales del Mundo</h3>
    </div>
    <div class="col-md-12 sidebar-opinion-content">
        <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
        <div class="media">
            <div class="media-left">
                <a href="<?php the_permalink(); ?>">
                    <?php
                               $terms = get_the_terms( $post->ID , 'content_taxonomy' );
                               foreach ($terms as $term) {
                                   if ($term->parent == 60){
                                       echo '<img src="'. get_bloginfo('template_url').'/images/logos/' . $term->slug . '.jpg" alt="' . $term->slug . '" class="media-img" />';
                                       $termname = $term->name;
                                   }
                               }
                    ?>
                </a>
            </div>
            <div class="media-body">
                <a href="<?php the_permalink(); ?>">
                    <h4 class="media-heading"><?php echo $termname; ?></h4>
                    <?php the_title(); ?>
                </a>
            </div>
        </div>
        <?php endwhile; ?>
        <div class="clearfix"></div>
        <?php wp_reset_postdata(); ?>
    </div>
</div>
<?php } ?>
