<?php
global $wpdb;
$result = $wpdb->get_results("SELECT * FROM wp_terms where name = ". "'" . $tag . "'");
foreach ($result as $item) { $slug = $item->slug; } $collapse = 1; ?>
<div class="sidebar-editorspick-container">
    <div class="sidebar-editorspick-bigtitle">
        <h3>Editor's Pick</h3>
    </div>
    <div class="sidebar-editorspick-title">
        <h2>Noticias Recomendadas</h2>
    </div>
    <div class="sidebar-editorspick-content">
        <?php $args = array( 'post_type' => 'post', 'posts_per_page' => $cantidad, 'tax_query' => array( array( 'taxonomy' => 'post_tag',
                                                                                                               'field' => 'slug', 'terms' => $slug ), ), ); ?>
        <?php query_posts( $args ); $y= 1; while ( have_posts() ) : the_post();  ?>
        <div class="panel-special "id="accordion" role="tablist" aria-multiselectable="true">
            <div>
                <div class="panel-heading panel-heading-editorspick" role="tab" id="heading<?php echo $collapse; ?>">
                    <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                </div>
            </div>
        </div>
        <?php $collapse++; endwhile; wp_reset_query(); ?>
    </div>
</div>
