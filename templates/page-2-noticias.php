<div class="container paddingb20" style="<?php echo $fondo; ?>">
    <div class="row">
        <div class="col-md-12 no-paddingl no-paddingr">
            <div class="container featured-main-container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 featured-main-content">
                        <div class="col-md-6 featured-article-1 no-paddingl">
                            <?php $post_id = $item[0]->item; ?>
                            <?php $queried_post = get_post($post_id); ?>
                            <?php $post_url = get_permalink($post_id); ?>
                            <?php $feat_image = $item[0]->thumbnail_url;//wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>
                            <?php $title = $queried_post->post_title; ?>
                            <?php $terms = get_the_terms($post_id, 'content_taxonomy'); ?>
                            <?php  if ( $terms != null ){ ?>
                            <?php foreach ( $terms as $term ) { $slug = $term->name; $mostrar = 1; } ?>
                            <?php  } else { $mostrar = 2; } ?>
                            <div class="featured-image-container">
                                <a href="<?php echo $post_url; ?>">
                                    <img src="<?php echo $feat_image ?>" alt="Sumarium - <?php echo $title; ?>">
                                </a>
                                <?php if ($mostrar == 1){ echo '<div class="img-content-tag">'. $slug .'</div>'; } ?>
                            </div>
                            <div class="col-md-12 no-paddingl no-paddingr">
                                <?php $post_id = $item[0]->item; ?>
                                <?php $queried_post = get_post($post_id); ?>
                                <?php $title = $queried_post->post_title; ?>
                                <?php $post_url = get_permalink($post_id); ?>
                                <h1 class="featured-bigtitle"><a href="<?php echo $post_url ?>"><?php echo $title; ?></a></h1>
                            </div>
                        </div>
                        <div class="col-md-6 featured-article-2 paddingl5 no-paddingr">
                            <?php $post_id = $item[1]->item; ?>
                            <?php $queried_post = get_post($post_id); ?>
                            <?php $post_url = get_permalink($post_id); ?>
                            <?php $feat_image = $item[1]->thumbnail_url;//wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>
                            <?php $title = $queried_post->post_title; ?>
                            <?php $terms = get_the_terms($post_id, 'content_taxonomy'); ?>
                            <?php  if ( $terms != null ){ ?>
                            <?php foreach ( $terms as $term ) { $slug = $term->name; $mostrar = 1; } ?>
                            <?php  } else { $mostrar = 2; } ?>
                            <div class="featured-image-container">
                                <a  href="<?php echo $post_url; ?>">
                                    <img src="<?php echo $feat_image ?>" alt="Sumarium - <?php echo $title; ?>">
                                </a>
                                <?php if ($mostrar == 1){ echo '<div class="img-content-tag">'. $slug .'</div>'; } ?>
                            </div>
                            <div class="col-md-12 no-paddingl no-paddingr">
                                <?php $post_id = $item[1]->item; ?>
                                <?php $queried_post = get_post($post_id); ?>
                                <?php $title = $queried_post->post_title; ?>
                                <?php $post_url = get_permalink($post_id); ?>
                                <h1 class="featured-bigtitle"><a href="<?php echo $post_url ?>"><?php echo $title; ?></a></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
