<div class="container">
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12 no-paddingl no-paddingr paddingb30">
            <section id="featured-pics">
                <div class="featured-pics">
                    <div class="col-md-12 no-paddingl no-paddingr">
                        <div class="featured-pics-title col-md-12 col-xs-12 col-sm-12 no-paddingl no-paddingr">
                            <h2>La jornada en imágenes</h2>
                        </div>
                        <div class="featured-pics-title2 col-md-12 col-xs-12 col-sm-12 no-paddingl no-paddingr"><a href="<?php echo home_url('/categoria/fotos/'); ?>">FOTOS</a></div>
                        <?php
wp_reset_query();
query_posts('taxonomy=category&category_name=Fotos&posts_per_page=12');
if( have_posts() ){
    while( have_posts() ){ the_post(); ?>
                        <div class="featured-pic-item col-md-2 col-xs-4 col-sm-2 no-paddingl no-paddingr ">
                            <a href="<?php the_permalink(); ?>" alt="Sumarium - <?php echo get_the_title() ?>">
                                <?php
                          if ( has_post_thumbnail() ) {
                              the_post_thumbnail( 'search_img', array( 'class'=>"featured-pic"));
                          } else {
                              $pic = get_post_meta(get_the_ID(), 'sum_e_url', true);
                              $title = get_post_meta(get_the_ID(), 'sum_e_caption', true);
                              if (!$pic == ""){
                                  echo '<img src="'. $pic. '" class="featured-pic" alt="Sumarium - ' . get_the_title() . '"/>';
                              } else {
                                  echo '<img src="' . get_bloginfo( 'template_url' ) . '/images/no_pic.gif" class="featured-pic img-responsive" alt="Sumarium - ' . get_the_title() . '"/>';
                              }
                          }
                                ?>

                            </a>
                        </div>

                        <?php
                         }
}
                        ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
