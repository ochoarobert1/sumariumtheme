<div class="container paddingb20" style="<?php echo $fondo; ?>">
    <div class="row">
        <div class="col-md-12 no-paddingl no-paddingr">
            <div class="container featured-main-container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 featured-main-content">
                        <div class="col-md-9 col-xs-12 col-sm-12 no-paddingl no-paddingr">
                            <?php $post_id = $item[0]->item; ?>
                            <?php $queried_post = get_post($post_id); ?>
                            <?php $post_url = get_permalink($post_id); ?>
                            <?php $feat_image = $item[0]->thumbnail_url;//wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>
                            <?php $title = $queried_post->post_title; ?>
                            <?php $terms = get_the_terms($post_id, 'content_taxonomy'); ?>
                            <?php if ( $terms != null ){ ?>
                            <?php foreach ( $terms as $term ) { $slug = $term->name; $mostrar = 1; }?>
                            <?php } else { $mostrar = 2; } ?>
                            <div class="featured-image-container">
                                <a href="<?php echo $post_url; ?>">
                                    <img src="<?php echo $feat_image ?>" alt="Sumarium - <?php echo $title; ?>" />
                                </a>
                                <?php if ($mostrar == 1){ echo '<div class="img-content-tag">'. $slug .'</div>'; }?>
                            </div>
                            <?php $post_id = $item[0]->item; ?>
                            <?php $queried_post = get_post($post_id); ?>
                            <?php $title = $queried_post->post_title; ?>
                            <?php $post_url = get_permalink($post_id); ?>
                            <h1 class="featured-bigtitle visible-sm visible-xs"><a href="<?php echo $post_url ?>"><?php echo $title; ?></a></h1>
                        </div>
                        <div class="col-md-3 col-xs-12 col-sm-12 no-paddingl no-paddingr">
                            <div class="col-md-12 col-sm-6 col-xs-6 featured-mini featured-mini1 no-paddingr">
                                <?php $post_id = $item[1]->item; ?>
                                <?php $queried_post = get_post($post_id); ?>
                                <?php $title = $queried_post->post_title; ?>
                                <?php $post_url = get_permalink($post_id); ?>
                                <?php $feat_image = $item[1]->thumbnail_url;//wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>
                                <?php $fa = ""; $slug = ""; ?>
                                <?php  $terms = get_the_terms($post_id, 'content_taxonomy'); ?>
                                <?php if ( $terms != null ){ foreach ( $terms as $term ) { $slug = $term->slug; } }  ?>
                                <?php switch ($slug) { case 'analisis': $fa = "pencil"; break; case 'opinion': $fa = "pencil"; break;case 'fotos': $fa = "camera"; break;
                                                      case 'infografia': $fa = "area-chart"; break; case 'video': $fa = "play"; break; default: $fa = ""; } ?>
                                <div class="featured-image-container">
                                    <a href="<?php echo $post_url; ?>">
                                        <img src="<?php echo $feat_image ?>"  alt="Sumarium - <?php echo $title; ?>" />
                                    </a>
                                    <?php if ($fa != ""){ echo '<div class="img-content-tag-small"><i class="fa fa-'. $fa .'"></i></div>'; } ?>
                                </div>
                                <a href="<?php echo $post_url ?>"><?php echo $title; ?>&nbsp;<a class="plus" href="<?php echo $post_url ?>">+</a></a>
                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-6 featured-mini featured-mini2 no-paddingr">
                                <?php $post_id = $item[2]->item; ?>
                                <?php $queried_post = get_post($post_id); ?>
                                <?php $title = $queried_post->post_title; ?>
                                <?php $post_url = get_permalink($post_id); ?>
                                <?php $feat_image = $item[2]->thumbnail_url;//wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>
                                <?php $fa = ""; $slug = ""; ?>
                                <?php  $terms = get_the_terms($post_id, 'content_taxonomy'); ?>
                                <?php if ( $terms != null ){ foreach ( $terms as $term ) { $slug = $term->slug; } }  ?>
                                <?php switch ($slug) { case 'analisis': $fa = "pencil"; break; case 'opinion': $fa = "pencil"; break;case 'fotos': $fa = "camera"; break;
                                                      case 'infografia': $fa = "area-chart"; break; case 'video': $fa = "play"; break; default: $fa = ""; } ?>
                                <div class="featured-image-container">
                                    <a href="<?php echo $post_url; ?>">
                                        <img src="<?php echo $feat_image ?>"  alt="Sumarium - <?php echo $title; ?>" />
                                    </a>
                                    <?php if ($fa != ""){ echo '<div class="img-content-tag-small"><i class="fa fa-'. $fa .'"></i></div>'; } ?>
                                </div>
                                <a href="<?php echo $post_url ?>"><?php echo $title; ?>&nbsp;<a class="plus" href="<?php echo $post_url ?>">+</a></a>
                            </div>
                        </div>
                        <div class="col-md-12 visible-md visible-lg no-paddingl no-paddingr">
                            <?php $post_id = $item[0]->item; ?>
                            <?php $queried_post = get_post($post_id); ?>
                            <?php $title = $queried_post->post_title; ?>
                            <?php $post_url = get_permalink($post_id); ?>
                            <h1 class="featured-bigtitle"><a href="<?php echo $post_url ?>"><?php echo $title; ?></a></h1>
                        </div>
                        <div class="col-md-12 featured-extra-news no-paddingl no-paddingr">
                            <div class="col-md-3 col-xs-12 col-sm-3 text-center">
                                <img src="<?php bloginfo('template_url')?>/images/more.png" alt="ademas" class="img-responsive img-ademas" />
                            </div>
                            <div class="col-md-9 col-xs-12 col-sm-9">
                                <div class="col-md-12">
                                    <div class="col-md-4 col-xs-12 col-sm-12 featured-extra-block4">
                                        <?php $post_id = $item[3]->item; ?>
                                        <?php $queried_post = get_post($post_id); ?>
                                        <?php $title = $queried_post->post_title; ?>
                                        <?php $date = $queried_post->post_date; ?>
                                        <?php $post_url = get_permalink($post_id); ?>
                                        <?php $short_title = substr($title,0,55); ?>
                                        <?php $slug = ""; $fa = ""; ?>
                                        <?php $terms = get_the_terms($post_id, 'content_taxonomy'); ?>
                                        <?php if ( $terms != null ){ foreach ( $terms as $term ) { $slug = $term->slug; } } ?>
                                        <?php switch ($slug) { case 'analisis': $fa = "pencil"; break; case 'opinion': $fa = "pencil"; break;case 'fotos': $fa = "camera"; break;
                                                              case 'infografia': $fa = "area-chart"; break; case 'video': $fa = "play"; break; default: $fa = ""; } ?>
                                        <a href="<?php echo $post_url ?>"><?php echo $title; ?></a>
                                        <?php if ($fa != ""){ echo '<div class="img-content-tag-small"><i class="fa fa-'. $fa .'"></i></div>'; } $fa = ""; ?>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 featured-extra-block4">
                                        <?php $post_id = $item[4]->item; ?>
                                        <?php $queried_post = get_post($post_id); ?>
                                        <?php $title = $queried_post->post_title; ?>
                                        <?php $date = $queried_post->post_date; ?>
                                        <?php $post_url = get_permalink($post_id); ?>
                                        <?php $short_title = substr($title,0,55); ?>
                                        <?php $slug = ""; $fa = ""; ?>
                                        <?php $terms = get_the_terms($post_id, 'content_taxonomy'); ?>
                                        <?php if ( $terms != null ){ foreach ( $terms as $term ) { $slug = $term->slug; } } ?>
                                        <?php switch ($slug) { case 'analisis': $fa = "pencil"; break; case 'opinion': $fa = "pencil"; break;case 'fotos': $fa = "camera"; break;
                                                              case 'infografia': $fa = "area-chart"; break; case 'video': $fa = "play"; break; default: $fa = ""; } ?>
                                        <a href="<?php echo $post_url ?>"><?php echo $title; ?></a>
                                        <?php if ($fa != ""){ echo '<div class="img-content-tag-small"><i class="fa fa-'. $fa .'"></i></div>'; } $fa = ""; ?>
                                    </div>
                                    <div class="col-md-4 col-xs-12 col-sm-12 featured-extra-block4">
                                        <?php $post_id = $item[5]->item; ?>
                                        <?php $queried_post = get_post($post_id); ?>
                                        <?php $title = $queried_post->post_title; ?>
                                        <?php $date = $queried_post->post_date; ?>
                                        <?php $post_url = get_permalink($post_id); ?>
                                        <?php $short_title = substr($title,0,55); ?>
                                        <?php $slug = ""; $fa = ""; ?>
                                        <?php $terms = get_the_terms($post_id, 'content_taxonomy'); ?>
                                        <?php if ( $terms != null ){ foreach ( $terms as $term ) { $slug = $term->slug; } } ?>
                                        <?php switch ($slug) { case 'analisis': $fa = "pencil"; break; case 'opinion': $fa = "pencil"; break;case 'fotos': $fa = "camera"; break;
                                                              case 'infografia': $fa = "area-chart"; break; case 'video': $fa = "play"; break; default: $fa = ""; } ?>
                                        <a href="<?php echo $post_url ?>"><?php echo $title; ?></a>
                                        <?php if ($fa != ""){ echo '<div class="img-content-tag-small"><i class="fa fa-'. $fa .'"></i></div>'; } $fa = ""; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
