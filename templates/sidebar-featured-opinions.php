
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fechats = explode('-',$fecha);
    $today['year'] = $fechats[0];
    $today['mon'] = $fechats[1];
    $today['mday'] = $fechats[2];
} else {
    $today = getdate();
}
$args=array(
    'date_query' => array(
        array(
            'year'  => $today['year'],
            'month' => $today['mon'],
            'day'   => $today['mday'],
        ),
    ),
    'content_taxonomy' => 'cartas-al-editor');
query_posts( $args );
// The Loops
$y= 1;
if (have_posts() ) { ?>
<div class="sidebar-featured-opinions-container col-xs-6 col-sm-6">
    <div class="sidebar-featured-opinions-content-mask"></div>
    <div class="sidebar-featured-opinions-title">
        <h2>CARTAS AL EDITOR</h2>
    </div>
    <div class="sidebar-featured-opinions-content">
        <?php while ( have_posts() ) : the_post(); ?>
        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>

        <?php
                    $y++;
                    endwhile;
        ?>
    </div>
</div>
<?php
                   }
wp_reset_query();

?>
