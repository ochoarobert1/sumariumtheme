<?php
$ids ="";
$collapse = 1;
date_default_timezone_set('America/Caracas');
$fecha = date("Y-m-d");
/* WHERE last_viewed like '%". $fecha ."%' */
//if (isset($cantidad)){
    $popular = $wpdb->get_results("SELECT postid FROM " . $wpdb->prefix . "popularpostssummary WHERE DATE_FORMAT(last_viewed, '%Y %m %d') = DATE_FORMAT('". $fecha ."', '%Y %m %d') ORDER BY pageviews DESC LIMIT ". $cantidad, ARRAY_A);
    $cont = $cantidad - 1;
// } else {
    // $popular = $wpdb->get_results("SELECT postid FROM " . $wpdb->prefix . "popularpostsdata ORDER BY pageviews DESC LIMIT 10", ARRAY_A);
    // $cont = 9;
// }

$resultado = count($popular);
if ($resultado < 2){
    $popular = $wpdb->get_results("SELECT postid FROM " . $wpdb->prefix . "popularpostssummary WHERE DATE_FORMAT(last_viewed, '%Y %m %d') = DATE_FORMAT(now() - INTERVAL 1 DAY, '%Y %m %d') ORDER BY pageviews DESC LIMIT ". $cantidad, ARRAY_A);
}



for ($y = 0; $y <= $cont; $y++) {
    if ($y == $cont){
        $ids[] = $popular[$y]['postid'];
    } else {
        $ids[] = $popular[$y]['postid'] . ', ';
    }

}
?>


<div class="sidebar-mostviewed-container">
    <div class="sidebar-mostviewed-bigtitle">
        <h3>Top Stories</h3>
    </div>
    <div class="sidebar-mostviewed-title">
        <h2>LOS + LEÍDOS HOY</h2>
    </div>
    <div class="sidebar-mostviewed-content">
        <?php
$today = getdate();
/*$args = array( 'post__in' => $ids, 'posts_per_page' => $cantidad, 'orderby' => 'date', 'order' => 'DESC');*/
$args = array(
    'post__in' => $ids,
    'posts_per_page' => $cantidad,
    'orderby' => 'date',
    'order' => 'DESC'
    );
query_posts( $args );
// The Loop
while ( have_posts() ) : the_post();
        ?>
        <a class="panel-title" href="<?php the_permalink(); ?>" target="_blank">
            <div class="panel-special">
                <div class="panel-heading">
                    <div class="panel-div-special col-md-12 col-xs-12 col-sm-12 no-paddingl no-paddingr">
                        <div class="col-md-2 col-xs-2 col-sm-2"><h4 class="panel-number"><?php echo $collapse; ?></h4></div>
                        <div class="col-md-10 col-xs-10 col-sm-10 panel-mostviewed-news"><h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4></div>
                    </div>
                </div>
            </div>
        </a>
        <?php
$collapse++;
endwhile;
// Reset Query
wp_reset_query();
        ?>
    </div>
</div>
