<?php $collapse = 1; ?>
<div class="sidebar-mostrecent-container">
    <div class="sidebar-mostrecent-bigtitle">
        <h3>Recent Stories</h3>
    </div>
    <div class="sidebar-mostrecent-title">
        <h2>LAS NOTICIAS + RECIENTES</h2>
    </div>
    <div class="sidebar-mostrecent-content">
        <?php
            if (isset($cantidad)){
                $args = array('posts_per_page' => $cantidad, 'orderby' => 'date', 'order' => 'DESC' );
            }else {
                $args = array('posts_per_page' => 10, 'orderby' => 'date', 'order' => 'DESC' );
            }

            query_posts( $args );
            // The Loop
            while ( have_posts() ) : the_post();
        ?>
        <a class="panel-title" href="<?php the_permalink(); ?>" target="_blank">
            <div class="panel-special">
                <div class="panel-heading">
                    <div class="panel-div-special col-md-12">
                        <div class="col-md-12 no-paddingl no-paddingr"><h5><?php echo 'Hace '. themeblvd_time_ago(); ?></h5><h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4></div>
                    </div>
                </div>
            </div>
        </a>
        <?php
$collapse++;
endwhile;
// Reset Query
wp_reset_query();
        ?>
    </div>
</div>
