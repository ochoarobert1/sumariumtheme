<div class="three-cols-front-list">
    <div class="row">
        <?php
global $wpdb;
// $bloque Columna 1
$itemscol1 = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "pnt24_item WHERE id_lugar = " . $bloque->id_lugar . " AND columna = 2 ORDER BY posicion ASC");

if (count($itemscol1) > 0) {
    //echo 'tiene '.count($col1).' posts';
        ?>
        <?php
    foreach ($itemscol1 as $itemcol1) {
        //obtener datos del post
        $post = get_post($itemcol1->item);
        $titulo = $post->post_title;
        $autor = $post->post_author;
        $user_info = get_userdata($autor);
        $noimg = "";
        ?>
        <div class="col-md-12 no-paddingl no-paddingr three-cols-front-item" style="background: <?php echo $itemcol1->fond_color; ?>">
            <article>
                <a class="hover-link" href="<?php the_permalink(); ?>">
                    <?php if (!empty($itemcol1->thumbnail_url)) { ?>
                    <div class="three-cols-img-wrapper">
                        <img src="<?php echo $itemcol1->thumbnail_url; ?>" alt="Sumarium - <?php echo $titulo; ?>" />
                        <div class="three-cols-img-wrapper-mask"></div>
                    </div>
                    <?php } ?>
                    <div class="three-cols-title <?php echo 'three-cols-'.$noimg; ?>">
                        <header>
                            <a class="front-link" href="<?php the_permalink(); ?>">
                                <h1 style="color: <?php echo $itemcol1->text_color; ?>" class="three-cols-item-title <?php echo $noimg; ?>"><?php echo $titulo; ?></h1>
                            </a>
                        </header>
                    </div>
                </a>
            </article>
        </div>
        <?php } ?>
        <?php }  ?>
    </div>
</div>
