<div class="five-cols-front-list">
    <div class="row">
        <?php
global $wpdb;
// $bloque Columna 1
$itemscol1 = $wpdb->get_results(
    "
                                                SELECT * FROM " . $wpdb->prefix . "pnt24_item
                                                WHERE id_lugar = " . $bloque->id_lugar . "
                                                AND columna = 1 ORDER BY posicion ASC
                                            "
);
if (count($itemscol1) > 0) {
    //echo 'tiene '.count($col1).' posts';
        ?>
        <?php
    foreach ($itemscol1 as $itemcol1) {
        //obtener datos del post
        $slug = "";
        $estilo = "";
        $post = get_post($itemcol1->item);
        $titulo = $post->post_title;
        $autor = $post->post_author;
        $user_info = get_userdata($autor);
        $noimg = "";
        $terms = get_the_terms($itemcol1->item, 'content_taxonomy');
        if ( $terms != null ){
            foreach ( $terms as $term ) {
                $slug = $term->slug;
            }
        }
        switch ($slug) {
            case 'analisis':
                $estilo = "pencil";
                break;
            case 'opinion':
                $estilo = "pencil";
                break;
            case 'fotos':
                $estilo = "camera";
                break;
            case 'infografia':
                $estilo = "area-chart";
                break;
            case 'video':
                $estilo = "play";
                break;
            default:
                $estilo = "";
                break;
        }
        ?>
        <div class="col-md-12 five-cols-front-item">
            <article>

                <a class="hover-link" href="<?php the_permalink(); ?>">
                    <?php
                        if ($estilo != ""){
                            echo '<div class="img-content-tag-small-five-cols"><i class="fa fa-'. $estilo .'"></i></div>';
                        }
                    ?>
                    <?php if (!empty($itemcol1->thumbnail_url)) { ?>
                    <div class="five-cols-img-wrapper">
                        <img src="<?php echo $itemcol1->thumbnail_url; ?>" alt="Sumarium - <?php echo $titulo; ?>" />

                    </div>
                    <?php } else {
                        $noimg = "front-link-noimg";/*
                        $feat_image = get_post_meta($itemcol1->item, 'sum_e_url', true);
                        if ($feat_image == ""){
                            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($itemcol1->item) );
                            if ($feat_image == ""){

                            } else {
                                echo '<div class="five-cols-img-wrapper">';
                                echo '<img src="' .$feat_image . '" alt="Sumarium" />';
                                echo '</div>';
                                $noimg = "front-link-wimg";
                            }
                        } else {
                            echo '<div class="five-cols-img-wrapper">';

                            echo '</div>';
                            $noimg = "front-link-wimg";
                        }*/
                    } ?>

                    <div class="five-cols-title <?php echo 'five-cols-'.$noimg; ?>">
                        <header>
                            <a class="front-link" href="<?php the_permalink(); ?>">
                                <h1 class="five-cols-item-title <?php echo $noimg; ?>"><?php echo $titulo; ?></h1>
                                <br />
                                <h3 class="five-cols-item-author <?php echo $noimg; ?>">Por: <?php echo $user_info->display_name; ?></h3>
                            </a>
                        </header>
                    </div>


                </a>
            </article>
        </div>
    </div>
    <div class="row">
        <?php if (!empty($itemcol1->publicidad)) { ?>
        <div class="col-md-12 five-cols-ads-frame">
            <?php  /* the_ad_group($itemcol1->publicidad); */ ?>
        </div>
        <?php } // Fin publicidad ?>
        <?php } ?>
        <?php }  $estilo = ""; ?>
    </div>
</div>
