<?php $ids = ""; $collapse = 1; $ant = 0; date_default_timezone_set('America/Caracas'); $fecha = date("Y-m-d"); ?>
<div id="sticker-sidebar-xs" class="sidebar-related-container2">
    <div class="sidebar-related-bigtitle2">
        <div class="col-md-10 col-xs-7 visible-md visible-lg tags-section-title2 showdiv">
            <h3>Pendientes</h3>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 sidebar-related-title2 no-paddingl no-paddingr">
        <div id="sidebar-tag-title" class="col-md-10 no-paddingl no-paddingr">
            <h2><?php echo $tagname; ?></h2>
        </div>
    </div>
    <div id="tags-content" class="col-md-12 col-sm-12 col-xs-12 sidebar-related-content2 no-paddingl no-paddingr">
        <?php $args = array( 'tag' => $tags, 'posts_per_page' => -1, 'orderby' => 'date', 'order' => 'DESC' ); query_posts( $args ); while ( have_posts() ) : the_post(); if ($ant == 0){ $ant = get_the_ID(); } ?>
        <a id="<?php the_ID(); ?>" href="#noticia<?php echo $collapse; ?>" onclick="show_taged_xs(this.id, <?php echo $ant; ?>);">
            <div class="related-content2 col-md-12 no-paddingr no-paddingl">
                <div class="col-md-12 tag-news-title2 no-paddingr"><h4 id="title-<?php the_ID(); ?>" class="<?php if ($collapse == 1){ echo "activo"; }?>"><?php the_title(); ?></h4></div>
            </div>
        </a>
        <?php $ant = get_the_ID(); $collapse++; endwhile; wp_reset_query(); ?>
    </div>
                <div class="bar-xs-tap-2">
                <div id="tags-wrapper-xs" class="button-xs-tap-2 visible-xs">
                    <button class="btn btn-sm btn-danger">tap para nota</button>
                </div>
            </div>
</div>
