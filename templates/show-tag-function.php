<?php
require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
$id = $_POST['id'];
global $post;
query_posts( 'p='.$id );
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article>

        <div class="col-md-12 the-post-single">
            <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); ?>
            <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
            <?php echo '<div class="bbc-bar col-md-12"><img src="'. get_bloginfo('template_url').'/images/bbc-bar.png" alt="bbc-bar" class="" /></div>'; ?>
            <?php } } ?>
            <div class="col-md-12">
                <div class="single-title">
                    <h1><?php the_title(); ?></h1>
                    <?php $files = get_post_meta( get_the_ID() , 'sum_extracto', false ); $var = array_shift($files); ?>
                    <?php foreach ( $var as $info ) { echo "<div class='single-excerpt col-md-11 col-md-offset-1'><p>" . $info . "</p></div><div class='clearfix'></div>"; } ?>
                </div>
                <?php
$pic = get_post_meta(get_the_ID(), 'sum_e_url', true);
$title = get_post_meta(get_the_ID(), 'sum_e_caption', true);
if (!$pic == ""){
    echo '<div class="single-img"><img src="'. $pic. '" class="img-responsive"/>'; echo '<span class="single-img-text">' . $title . '</span>'; echo '</div>';
}
                ?>
                <div class="col-md-2 col-xs-4 col-sm-3">
                    <div class="addthis_sharing_toolbox"></div>
                </div>
                <div class="col-md-10 col-xs-8 col-sm-9 author-text">
                    <?php $correo = get_the_author_meta( user_email, $userID );?>
                    <?php $twitter = get_the_author_meta( twitter, $userID );?>
                    Publicada por: <span itemprop="creator"><strong><?php the_author(); ?></strong></span> el <span itemprop="datePublished"><?php the_date(); ?> - <?php the_time(); ?></span><span class="meta-block"><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $correo; ?>"><?php echo $correo; ?></a></span>
                    <?php if(!empty($twitter)){ ?><span class="meta-block"><i class="fa fa-twitter"></i> <a href="https://www.twitter.com/<?php echo $twitter; ?>" target="_blank">@<?php echo $twitter; ?></a></span><?php } ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 no-paddingr no-paddingl single-info">
                    <hr />
                    <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); ?>
                    <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
                    <?php echo '<a href="http://www.bbcmundo.com/" target="_blank"><div class="clearfix"></div><span class="bbc-bar3"><img src="'. get_bloginfo('template_url').'/images/bbc-single.gif" alt="bbc-footer" class="" /></span></a>'; ?>
                    <?php } } ?>
                    <?php the_content(); ?>
                    <div class="clearfix"></div>
                    <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); ?>
                    <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
                    <?php echo '<div class="bbc-bar2 col-md-12"><img src="'. get_bloginfo('template_url').'/images/bbc-footer.gif" alt="bbc-footer" class="" /></div>'; ?>
                    <?php } } ?>
                    <div class="clearfix"></div>
                    <div class="category-box col-md-10 col-md-offset-1 no-paddingr no-paddingl" itemprop="keywords">
                        Categoría: <?php $categories = get_the_category(); $separator = ' '; $output = ''; ?>
                        <?php if($categories){ ?>
                        <?php foreach($categories as $category) { $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator; } ?>
                        <?php echo trim($output, $separator); ?>
                        <?php } ?>
                        <?php if (has_tag()){ echo "| Claves: "; } ?>
                        <?php $posttags = get_the_tags(); ?>
                        <?php if ($posttags) { foreach($posttags as $tag) { echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a> '; } }?>
                    </div>
                </div>
            </div>
        </div>

</article>
<?php endwhile; else : ?>
<p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
