<!DOCTYPE html>
<html lang="es_ES">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php if(is_home()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?></title>
        <?php /* $current_url = $_SERVER['REQUEST_URI']; $clean_url = str_replace('wp_sumarium/', '', $current_url); $clean_url = str_replace('/', '', $clean_url); echo $clean_url; */ ?>
        <?php $current_url = $_SERVER['REQUEST_URI']; $clean_url = str_replace('/', '', $current_url); ?>
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = get_excerpt_by_id($my_posts[0]->ID); echo '<meta name="description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta name="description" content="Sumarium.com te trae las últimas noticias de todo el mundo, que cubre noticias de última hora en los negocios, la política, la tecnología, y más en vídeo e imágenes.">'; } ?>
        <?php if (is_page() ) { echo '<meta name="description" content="Sumarium.com te trae las últimas noticias de todo el mundo, que cubre noticias de última hora en los negocios, la política, la tecnología, y más en vídeo e imágenes.">'; } ?>
        <meta name="title" content="<?php if(is_home()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?>">
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $terms = get_the_terms( $my_posts[0]->ID, 'post_tag' ); if ( $terms && ! is_wp_error( $terms ) ) : $draught_links = array(); ?>
        <?php  foreach ( $terms as $term ) { $draught_links[] = $term->name; } $on_draught = join( ", ", $draught_links ); endif;
                               echo '<meta name="keywords" content="Sumarium, Sumarium.com, Sumarium Noticias, noticias de venezuela, Noticias24, '. $on_draught .'" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() || is_page() ) { echo '<meta name="keywords" content="Sumarium, Sumarium.com, Sumarium Noticias, noticias de venezuela, Noticias24" />'; } ?>
        <meta name="author" content="Grupo Sumarium" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@sumariumcom" />
        <meta name="twitter:creator" content="@sumariumcom" />
        <meta property='fb:admins' content='100000133943608'/>
        <meta property='fb:admins' content='1495142675'/>
        <meta property='fb:admins' content='1630413312'/>
        <meta property="fb:app_id" content="506348762838809"/>
        <meta property="og:title" content="<?php if(is_home()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, right); echo bloginfo("name"); } ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="<?php if(is_single()) { the_permalink(); } else { echo 'http://www.sumarium.com/'; }?>" />
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = get_excerpt_by_id($my_posts[0]->ID); echo '<meta property="og:description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta property="og:description" content="Sumarium.com te trae las últimas noticias de todo el mundo, que cubre noticias de última hora en los negocios, la política, la tecnología, y más en vídeo e imágenes.">'; } ?>
        <?php if (is_page() ) { echo '<meta property="og:description" content="Sumarium.com te trae las últimas noticias de todo el mundo, que cubre noticias de última hora en los negocios, la política, la tecnología, y más en vídeo e imágenes.">'; } ?>
        <meta property="og:image" content='<?php if(is_single()){ $pic = get_post_meta(get_the_ID(), 'sum_e_url', true); if ($pic == "") { echo bloginfo('template_url')."/images/oglogo.png";} else {echo $pic;} } else { echo bloginfo('template_url')."/images/oglogo.png"; } ?>' />
        <meta name="alexaVerifyID" content="ZBbxb76DiRUpzOaeKdPQb6RSGgA"/>
        <link rel="alternate" hreflang="es" href="http://sumarium.com/" />
        <?php if (is_single()){ $pic = get_post_meta(get_the_ID(), 'sum_e_url', true); if (!$pic == ""){ echo '<meta property="og:image" content="'. $pic. '" />'; } } ?>
        <?php if(is_front_page()) { echo '<META HTTP-EQUIV="refresh" CONTENT="300">'; }; ?>
        <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.ico" />
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/style.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/main-core-style.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/special-functions.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/sumarium-style.css" type="text/css" />
        <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/sumarium-mediaqueries.css" type="text/css" />
        <link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=PT+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
        <?php if(!is_front_page()) { echo '
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54345578267dfb76" async></script>'; } ?>
        <!--[if IE]> <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]>  <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                    appId      : '506348762838809',
                    xfbml      : true,
                    version    : 'v2.2'
                });
            };
            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/es_ES/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <?php wp_head(); ?>
        <meta name="google-site-verification" content="Kl_AHXKfUQXQzP2TalcezseMIueJqDzA-DEieKIBg7I" />
    </head>
    <body id="main">
        <div id="fb-root"></div>
        <header>
            <!-- N24_Portada_1000x250 -->
            <?php global $wpdb; ?>
            <?php /* $pub = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."pnt24_into_sidebar WHERE id_insidebar = 1" ); ?>
            <?php if($pub[0]->id_ad_group != 0){ ?>
            <div class="top-ads-wrapper">
                <div class="container nosidepadding">
                    <div class="col-md-12 nosidepadding">
                        <div class="top-ads-container">
                            <?php the_ad_group($pub[0]->id_ad_group); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } */?>
            <div id="top-navbar" class="visible-md visible-lg top-navbar opacity1 delay-one">
                <div class="container">
                    <div class="row">
                        <div class="logo col-md-3 no-paddingl no-paddingr"> <a href="<?php bloginfo('url'); ?>"> <img src="<?php bloginfo('template_url');?>/images/logo.png" alt="Sumarium Logo" /> </a> </div>
                        <div class="navs col-md-8 no-paddingr no-paddingl">
                            <?php wp_nav_menu( array('menu' => 'menu-nav','menu_class' => 'menu-nav', )); ?>
                        </div>
                        <div class="navs-f navs col-md-1 no-paddingr no-paddingl">
                            <div class="twitter-container">
                                <ul id="menu-menu-nav" class="menu-nav">
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-338"><a href="https://www.facebook.com/pages/Sumariumcom/1543496632565607" target="_blank"><i class="fa fa-facebook icons-social-header"></i></a></li>
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-339"><a href="http://www.twitter.com/sumariumcom" target="_blank"><i class="fa fa-twitter icons-social-header"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- RESPONSIVE SMALL MENU -->
            <div id="top-navbar-mobile" class="opacity1 delay-one visible-xs visible-sm no-paddingl no-paddingr">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo home_url('/'); ?>" title="<?php bloginfo('name'); ?>"> <img src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Sumarium - Logo" class="navbar-logo"> </a> </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <?php wp_nav_menu( array('menu' => 'menu-nav','menu_class' => 'menu-nav nav navbar-nav mini navbar-right', )); ?>
                        </div>
                    </div>
                </nav>
            </div>
        </header>
        <?php //echo "SELECT * FROM ".$wpdb->prefix."pnt24_pending left join ".$wpdb->prefix."pnt24_into_pending using(id_peding) left join ".$wpdb->prefix."terms using(term_id) where id_lugar = 0"; ?>
        <?php $elements = $wpdb->get_results(" SELECT * FROM ".$wpdb->prefix."pnt24_pending left join ".$wpdb->prefix."pnt24_into_pending using(id_pending) left join ".$wpdb->prefix."terms using(term_id) where id_pending = 1"); ?>
        <?php if ($elements[0]->active == '1'){ ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 no-paddingr no-paddingl">
                    <div class="nav-tags">
                        <div class="menu-menu-tags-container">
                            <ul class="menu-menu-tags">
                                <?php foreach($elements as $term){ ?>
                                <a href="<?php echo home_url('/');?>tag/<?php echo $term->slug; ?>"><?php echo $term->name;?></a>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php $element = $wpdb->get_results(" SELECT * FROM ".$wpdb->prefix."pnt24_urgent"); ?>
        <?php if($element[0]->active == '1'){?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 no-paddingl no-paddingr urgent-text">
                    <?php if(!empty($element[0]->link)){?>
                    <a href="<?php echo $element[0]->link;?>" target="_blank">
                        <?php echo $element[0]->title;?></a>
                    <?php }else {?>
                    <p><?php echo $element[0]->title;?></p>
                    <?php }?>
                </div>
            </div>
        </div>
        <?php } ?>
