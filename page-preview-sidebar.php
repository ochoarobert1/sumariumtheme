<?php get_header(); ?>
<?php
$block = $_GET['bloque'];
$zon = $_GET['zon'];

$posicion_r = $_GET['posicion_r'];
$cantidad_r = $_GET['cantidad_r'];
$activar_r = $_GET['activar_r'];
	
$posicion_p = $_GET['posicion_p'];
$cantidad_p = $_GET['cantidad_p'];
$activar_p = $_GET['activar_p'];
	
$posicion_e = $_GET['posicion_e'];
$cantidad_e = $_GET['cantidad_e'];
$activar_e = $_GET['activar_e'];
$tags_e = $_GET['tags_e'];

$sidebar[$posicion_r]->posicion_r = $posicion_r;
$sidebar[$posicion_r]->cantidad_r = $cantidad_r;
$sidebar[$posicion_r]->activar_r = $activar_r;

$sidebar[$posicion_p]->posicion_p = $posicion_p;
$sidebar[$posicion_p]->cantidad_p = $cantidad_p;
$sidebar[$posicion_p]->activar_p = $activar_p;

$sidebar[$posicion_e]->posicion_e = $posicion_e;
$sidebar[$posicion_e]->cantidad_e = $cantidad_e;
$sidebar[$posicion_e]->activar_e = $activar_e;

$cantidad_mini = $_GET['cantidad_mini'];

for($i = 0; $i < $cantidad_mini; $i++){

	$posicion_m = $_GET['posicion_m'.$block.'_n'.$i];
	
	$sidebar[$posicion_m]->cantidad = $_GET['cantidad_m'.$block.'_n'.$i];
	$sidebar[$posicion_m]->activar = $_GET['activar_m'.$block.'_n'.$i];
	$sidebar[$posicion_m]->tags = $_GET['tags_m'.$block.'_n'.$i];
	$sidebar[$posicion_m]->url = $_GET['url'.$block.'_n'.$i];
}
?>

<?php
//QUERY PARA TRAER LAS ZONAS ACTIVAS EN ORDEN DE POSICION
global $wpdb;
$query = $wpdb->get_results("SELECT id_zona FROM ". $wpdb->prefix ."pnt24_zona left join ". $wpdb->prefix ."pnt24_lugar using(id_zona) where id_lugar = ".$zon);
$zonas = $wpdb->get_results(
    '
            SELECT * FROM ' . $wpdb->prefix . 'pnt24_zona
            WHERE id_zona = '. $query[0]->id_zona .'
        '
);
global $wpdb;

$nzonas = count($zonas);
$clase = "";
$z = 1; //contador de zonas
$d = 1; // contador para cintillo
$u = 1; // contador de urgente
foreach ($zonas as $zona) {
    if ($z > 1){ $clase = "lazyload"; }
?>
<div id="zona<?php echo $z; ?>" class="zona <?php echo $clase; ?>">
    <!-- ZONAS -->
    <?php
                           //HACER EL QUERY PARA LOS DESTACADOS
                           global $wpdb;
                           $destacados = $wpdb->get_results(
                               '
                SELECT * FROM ' . $wpdb->prefix . 'pnt24_lugar
                WHERE id_zona = ' . $zona->id_zona . '
                AND TIPO = 1
                AND activo = 1
                ORDER BY id_zona
                LIMIT 1
            '
                           );

                           foreach ($destacados as $destacado) {

                               //hacer la consulta para los posts
                               global $wpdb;
                               $item = $wpdb->get_results(
                                   '
                    SELECT * FROM ' . $wpdb->prefix . 'pnt24_item
                    WHERE id_lugar = ' . $destacado->id_lugar . '
                '
                               );


                               if (!empty($destacado->fondo_url)) {
                                   $fondo = "width:100%;background-size:cover;background-image: url(" . $destacado->fondo_url . ");";
                               } else {
                                   $fondo = '';
                               }
                               $l = 1; //contador de lugares
                               // MOSTRARNDO DESTACADOS DE LA ZONA
    ?>
    <?php
                               $lugar = 'templates/page-' . $destacados[0]->plantilla . '-noticias.php';
                               include(locate_template($lugar));
                               if ($d == 1){
                                   include(locate_template('templates/ad-thin-fullwidth.php'));
                                   $d++;
                               }
    ?>
    <?php
                           } //FIN DESTACADO
    ?>
    <div class="clearfix"></div>

    <?php
                           //HACER EL QUERY PARA LOS BLOQUES
                           global $wpdb;
                           $bloques = $wpdb->get_results(
                               '
                SELECT * FROM ' . $wpdb->prefix . 'pnt24_lugar
                WHERE id_zona = ' . $zona->id_zona . '
                AND TIPO = 2
                AND activo = 1
                ORDER BY id_zona
                LIMIT 1
            '
                           );
                           // MOSTRARNDO BLOQUE DE LA ZONA
                           foreach ($bloques as $bloque) {
                               //echo var_dump($bloque);
                               #  Esqueleto para la parte inferior de la seccion completa
                               #  Este esqueleto va luego del frame de destacados en cada seccion
                               /* Tiene 3 columnas, de izquierda a derecha:
              Columna 1: Noticias en formato de 5 columnnas
              Columna 2: Noticass en formato de 3 columnas
              columna 3: Sidebar para publicidad */
    ?>
    <section id="skeleton-loop">
        <div class="skeleton-loop">
            <div class="container">
                <div class="row">


                    <div class="col-md-8 nosidepadding">

                    <?php global $wpdb;
                         $titulo = $wpdb->get_results(" SELECT * FROM " . $wpdb->prefix . "pnt24_lugar WHERE id_zona = " . $zona->id_zona . " AND tipo = 2 AND activo = 1");
                         if (!empty($titulo[0]->fondo_url) || !empty($titulo[0]->texto)) {
                     ?>
                            <div class="col-md-12 nosidepadding block-front-title">
                                <?php if (!empty($titulo[0]->texto)) {?>
                                        <div class="col-md-3 col-xs-3 col-sm-3 no-paddingl no-paddingr separador-outside"></div>
                                        <div class="col-md-6 col-xs-6 col-sm-6 separador-inside no-paddingl no-paddingr" style="background:<?php echo $titulo[0]->fond_color; ?>;">
                                           <div class="col-md-12 separador-text" style="background:<?php echo $titulo[0]->fond_color; ?>; border: 1px solid <?php echo $titulo[0]->text_color;?>; color: <?php echo $titulo[0]->text_color;?>;">
                                               <?php echo $titulo[0]->texto;?>
                                           </div>
                                        </div>
                                        <div class="col-md-3 col-xs-3 col-sm-3 no-paddingl no-paddingr separador-outside"></div>
                                <?php } else { ?>
                                <?php /*if (!empty($titulo[0]->fondo_url)) {*/ ?>
                                    <img src="<?php echo $titulo[0]->fondo_url; ?>" alt="Sumarium" class="img-responsive" />
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>

                        <?php } ?>

                    <?php
                        $elements = $wpdb->get_results(" SELECT * FROM ".$wpdb->prefix."pnt24_pending left join ".$wpdb->prefix."pnt24_into_pending using(id_pending) left join ".$wpdb->prefix."terms using(term_id) where id_lugar = ".$bloque->id_lugar);
                        if ($elements[0]->active == '1'){
                    ?>
                            <div class="col-md-12 nosidepadding block-front-title block-tags-container">
                                <ul class="block-tags-content">
                                    <?php foreach($elements as $term){ ?>
                                    <li class="block-item"><a href="<?php echo home_url('/');?>tag/<?php echo $term->slug; ?>"><div class="block-text"><?php echo $term->name;?></div></a></li>
                                    <?php } ?>
                                </ul>
                            </div>

                        <?php } ?>

                        <div class="col-md-7 no-paddingl">
                             <?php include(locate_template('templates/five-cols-front-item.php')); ?>
                        </div>
                        <div class="three-cols-main-container col-md-5 ">
                            <?php include(locate_template('templates/three-cols-front-item.php')); ?>
                        </div>

                    </div>

                    <div class="col-md-4 nosidepadding">
                        <div class="sidebar-section-loop">
							<div class="front-ads-container">
								<div class="col-md-12 ads-single">
									<div class="sidebar-front-page-news">
									<?php for($i = 1; $i <= 10; $i++){ ?>
										<?php if (array_key_exists("$i", $sidebar)) { ?>
											
											<?php if ($sidebar[$i]->activar_r == '1') { ?>
											  <div class="col-md-12 ads-single">
												<?php $cantidad = $sidebar[$i]->cantidad_r; include(locate_template('templates/sidebar-most-recent.php'));?>
											  </div>
											<?php } ?>
										  
											<?php if ($sidebar[$i]->activar_p == '1') { ?>
											  <div class="col-md-12 ads-single">
													<?php $cantidad = $sidebar[$i]->cantidad_p; include(locate_template('templates/sidebar-most-view.php'));?>
											  </div>
											<?php } ?>
										  
											<?php if ($sidebar[$i]->activar_e == '1') { ?>
											  <div class="col-md-12 ads-single">
												<?php $tag = $sidebar[$i]->tags_e; $cantidad = $sidebar[$i]->cantidad_e; include(locate_template('templates/sidebar-editorspick.php'));?>
											  </div>
											<?php } ?>
											
											<?php if ($sidebar[$i]->activar == '1') { ?>
												<div class="col-md-12 ads-single">
													<?php $url_fondo = $sidebar[$i]->url; $tag = $sidebar[$i]->tags; $cantidad = $sidebar[$i]->cantidad;  ?>
													<?php include(locate_template('templates/sidebar-ministories.php'));?>
												</div>
											<?php } ?>
											
										<?php } ?>
									<?php } ?>
									
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
                               //get_template_part('templates/skeleton-section-loop');
                           }//FIN BLOQUE
    ?>
    <?php
                           
                           if ($zona->pics == '1') {
                               get_template_part('templates/featured-pics');
                           }
						   
						   if ($zona->voces == '1') {
                               get_template_part('templates/featured-opinions');
                           }
    ?>
    <?php
                           //cargar el separador
                           if ($zona->separador > 0) {
    ?>
    <div class="container zoneads">
        <div class="row">
            <?php the_ad_group($zona->separador); ?>
        </div>
    </div>
    <?php }
    ?>
</div>
<?php
                           $z++;
                          }; //FIN ZONAS
?>
<?php get_footer(); ?>
