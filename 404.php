<?php get_header(); ?>
<div class="container main-content">
    <div class="row">
        <div class="col-md-8 col-xs-12 col-sm-12 padding-xs contenido no-paddingl">
           <h2>Error</h2>
           <h2>La página que buscas o el link del sitio está roto.</h2>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-12 visible-md visible-lg">
            <?php include(locate_template('templates/sidebar-most-recent.php'));?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
