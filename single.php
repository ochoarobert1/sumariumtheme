<?php get_header(); ?>
<?php the_post(); ?>
<?php $termeditorial = 0; $categories = get_the_category(); if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){ foreach ($categories as $categoria){ $cat = $categoria->name; } }?>
<?php $formato = get_post_meta( get_the_ID() , 'sum_single_format', true ); ?>
<?php if ($formato == 'parallax') {
    get_template_part('template-parallax-single');
} else {
    get_template_part('template-normal-single');
}
?>
<?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
<?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
<!-- start tracking code -->

<noscript><img src="http://sa.bbc.com/b/ss/bbcwglobalprod,bbcmundo/1/H.26.2--NS/0?c45=mundo&c25=sumarium.com" height="1" width="1" border="0" alt="" /></noscript><script language="JavaScript" type="text/javascript" src="http://static.bbci.co.uk/ws/js/vendor/site_catalyst/s_code_bbcws.js"></script>
<script>
    s_bbcws.prop25="sumarium.com";
    s_bbcws.prop45="mundo";
    s_bbcws.t();
</script>
<!-- end tracking code -->
<?php } } } ?>


<?php get_footer(); ?>
