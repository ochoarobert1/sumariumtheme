<?php get_header(); ?>
<div class="container main-content">
    <div class="row">

        <div class="col-md-12 col-xs-12 col-sm-12 padding-xs contenido no-paddingl no-paddingr">

            <?php
/* REMOVE AUTOP FROM CONTENT - EXCERPT */
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');


// The Loop
if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post();
$post_thumbnail_id = get_post_thumbnail_id();
$image_attributes = wp_get_attachment_image_src( $post_thumbnail_id, 'full' );
$width = $image_attributes[1];
$height = $image_attributes[2];
if ($width > $height){
    $clase = "archive-ancho-fotos";
}else{
    $clase = "archive-largo-fotos";
}
            ?>
            <article itemscope itemtype="http://schema.org/NewsArticle">
                <div class="col-md-6 col-xs-6 col-sm-6 no-paddingl">
                    <a href="<?php the_permalink() ?>">
                        <div class="col-md-12 col-xs-12 col-sm-12 archive-fotos-container">
                            <?php
                if ( has_post_thumbnail() ) {
                the_post_thumbnail( 'search_img', array( 'class'=>"img-responsive"));
            } else {
                $pic = get_post_meta(get_the_ID(), 'sum_e_url', true);
                $title = get_post_meta(get_the_ID(), 'sum_e_caption', true);
                if (!$pic == ""){
                    echo '<img src="'. $pic. '" class="img-responsive archive-largo-fotos" itemprop="image thumbnailURL" alt="Sumarium - '.get_the_title() .'"/>';
                } else {
                    echo '<img src="' . get_bloginfo( 'template_url' ) . '/images/no_pic.gif" class="archive-no-image" itemprop="image thumbnailURL" alt="Sumarium - '.get_the_title() .'"/>';
                }
            }
                            ?>
                            <div class="archive-fotos-title"><header><a href="<?php the_permalink(); ?>" itemprop="URL"><h1 itemprop="about"><?php the_title() ?></h1></a></header></div>
                        </div>
                    </a>
                </div>
            </article>
            <?php endwhile; ?>
            <div class="paginator col-md-7 col-xs-12 col-sm-7 col-md-offset-4 col-sm-offset-4">
                <?php wp_paginate(); ?>
            </div>
            <div class="clearfix"></div>
            <?php wp_reset_postdata(); ?>
            <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>

        </div>
    </div>
</div>
<?php
get_footer();
?>
