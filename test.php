<div class="timeline">
    <dl>
        <dt>Febrero 2015</dt>
        <!-- Derecha -->
        <dd class="pos-right clearfix">
            <div class="circ"></div>
            <div class="time">23</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <p>Copei se suscribe en el Acuerdo Nacional para la Transición, una iniciativa de los opositores María Corina Machado, Antonio Ledezma y Leopoldo López en la que se planteaba, entre otras cosas, “una agenda política-institucional dirigida a restituir las libertades conculcadas, la soberanía, la paz social y el Estado de Derecho; y una agenda para atender la emergencia social y asegurar la atención eficaz a los sectores más vulnerables”.</p>
                </div>
            </div>
        </dd>
        <dt class="dt-title">Denuncian invasión de sede del partido en Las Palmas.</dt>
        <!-- Derecha -->
        <dd class="pos-left clearfix">
            <div class="circ"></div>
            <div class="time">23</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <h4 class="events-heading">Roberto Enríquez:</h4>
                    <p>"Si están bravos en el gobierno porque el Copei pidió en esta semana la intervención del Sebin (…) Si están molestos porque hemos inciado acciones a través de un equipo jurídico encabezado por el doctor Zambrano en España contra el partido Podemos por el financiamiento ilegal con dinero de los venezolanos y estamos exigiendo el reembolso de ese dinero al patrimonio venezolano, y si están molestos porque hoy Copei junto a las esposas del alcalde Ledezma y a los firmantes, la familia de López y de María Corina Machado, hemos anunciado que la dirección nacional de Copei suscribe ese acuerdo nacional para la transición mañana y no vamos a parar (…) Si hay alguna acción legal contra nosotros, pues que la tomen"</p>
                </div>
            </div>
        </dd>
        <dt class="dt-title">Piden “ilegalización” del partido socialcristiano</dt>
        <!-- Derecha -->
        <dd class="pos-right clearfix">
            <div class="circ"></div>
            <div class="time">25/02/2015</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <h4 class="events-heading">CARITA ARGIMIRO APONTE:</h4>
                    <p>"El secretario general de Podemos, Argimiro Aponte, consignó ante el Consejo Nacional Electoral una solicitud de cancelación del registro del partido Copei. Alegó que la petición obedecía a que la tolda verde apoya el acuerdo nacional para la transición, que a juicio de la dirigencia de Podemos, intentaba derrocar al presidente Nicolás Maduro."</p>
                </div>
            </div>
        </dd>
        <!-- Izquierda -->
        <dd class="pos-left clearfix">
            <div class="circ"></div>
            <div class="time">18/03/2015</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <p>Roberto Enríquez solicitó al CNE declarar nula la petición de ilegalización del partido. Acudió al llamado del Consejo Nacional Electoral CNE, “para ser escuchados” luego de que el Polo Patriótico introdujo la solicitud de ilegalización de la tolda verde ante el ente comicial.</p>
                </div>
            </div>
        </dd>
        <!-- Derecha -->
        <dd class="pos-right clearfix">
            <div class="circ"></div>
            <div class="time">18/03/2015</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <h4 class="events-heading">Roberto Enríquez:</h4>
                    <p>“Estamos hablando que desde hace 60 años en este país no se iniciaba formalmente un proceso de ilegalización de una organización política. Venimos a exigirle al CNE, sin saber lo que nos van a decir, primero, que se nos respeten nuestra granatía constitucional a defeneder nuestras ideas políticas como no los permite la Constitución. Segundo, a exigirle al Polo Patriótico que se saque ese ADN fascista (…) Ellos nos están acusando de golpistas, y que por ser un partido golpista dedemos desaparecer. La semana pasada se nos citó en la Fiscalía, hoy se nos trae para acá para el CNE, venimos a dar la cara”</p>
                </div>
            </div>
        </dd>
        <!-- Izquierda -->
        <dd class="pos-left clearfix">
            <div class="circ"></div>
            <div class="time">27/7/2015</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <p>Los integrantes electos de la Dirección Política Nacional y presidentes de las mesas directivas estadales de Copei correspondientes a los estados Anzoátegui, Aragua, Delta Amacuro, Nueva Esparta, Táchira, Yaracuy y Zulia, introducen una acción de amparo en tutela de intereses colectivos contra lo que denominaron vías de hecho ejecutadas por la Dirección Política Nacional de ese partido.</p>
                </div>
            </div>
        </dd>
        <!-- Derecha -->
        <dd class="pos-right clearfix">
            <div class="circ"></div>
            <div class="time">30/07/2015</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <p>El TSJ nombra junta directiva Ad Hoc en el partido Copei, “Se ordenó la realización de la consulta estatutaria a las Direcciones Políticas Estadales de Copei sobre las postulaciones en los comicios parlamentarios del presente año, con carácter de urgencia, y (…) se ordenó al Consejo Nacional Electoral (CNE) abstenerse de aceptar cualquier postulación que no sea de las acordadas conforme a los procedimientos establecidos por la Mesa Directiva ad hoc designada (…) En cumplimiento del orden jurídico, el TSJ acordó las medidas cautelares solicitadas por los accionantes y, en consecuencia, acordó el nombramiento de una junta ad hoc integrada temporalmente por los accionantes miembros de ese partido, encabezada por Pedro Urrieta Figueredo, como presidente, suspendiendo provisionalmente la actual Mesa Directiva Nacional y la Dirección política Nacional de Copei”.</p>
                </div>
            </div>
        </dd>
        <dt class="dt-title">La MUD acordó por unanimidad no postular candidatos de Copei en la tarjeta única para las parlamentarias del 6-D.</dt>
        <!-- Izquierda -->
        <dd class="pos-left clearfix">
            <div class="circ"></div>
            <div class="time">4/8/2015</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <h4 class="events-heading">Jesús Torrealba</h4>
                    <p>“Copei está fuera de la MUD para hacer una suerte de cordón sanitario, evitar que esa intrusión del oficialismo logre penetrar incluso en las listas de la unidad ¿Cuál es nuestro deber como custodios de la unidad? preservarla de esa intrusión y evitar que la larga mano del régimen que está bien enguantada con un pretexto jurídico utilice a Copei como puerta de entrada para meterse dentro de la MUD”.</p>
                </div>
            </div>
        </dd>
        <!-- Derecha -->
        <dd class="pos-right clearfix">
            <div class="circ"></div>
            <div class="time">7/8/2015</div>
            <div class="events">
                <div class="pull-left">
                    <img class="events-object img-rounded" src="img/photo-1.jpg">
                </div>
                <div class="events-body">
                    <p>Nueva junta de Copei acusa a la MUD en un comunicado de no servir a la Unidad y ratifica que no inscribirá candidatos con tarjeta de Copei</p>
                </div>
            </div>
        </dd>
    </dl>
</div>
