<div class="container-fluid" itemscope itemtype="http://schema.org/NewsArticle">
    <div class="row">
        <?php $pic = get_post_meta(get_the_ID(), 'sum_e_url', true); $title = get_post_meta(get_the_ID(), 'sum_e_caption', true); ?>
        <div class="single-parallax col-md-12 no-paddingl nopaddingr" style="background:url('<?php echo $pic; ?>')">
            <?php if (!$title == "") { echo '<span class="single-parallax-text">' . $title . '</span>'; } ?>
        </div>
        <div class="container main-content">
            <div class="row">
                <div class="col-md-12 the-post-single no-paddingl no-paddingr">
                    <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
                    <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
                    <?php echo '<div class="bbc-bar col-md-12"><img src="'. get_bloginfo('template_url').'/images/bbc-bar.png" alt="bbc-bar" class="" /></div>'; ?>
                    <?php } } }?>
                    <div class="col-md-12">
                        <?php if ( in_category( 'opinion' )) { ?>
                        <div class="single-title">
                            <div class="single-avatar col-md-2 no-paddingl no-paddingr">
                                <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
                                <?php foreach ($terms as $term) { if ($term->parent == 60){ ?>
                                <?php echo '<img src="'. get_bloginfo('template_url').'/images/logos/' . $term->slug . '.jpg" alt="' . $term->slug . '" class="media-img-editorial" />'; ?>
                                <?php $termname = $term->name; $termeditorial = 1; } } } ?>
                                <?php if ($termeditorial == 0){ echo get_avatar( get_the_author_meta('ID'), "120"); } ?>
                            </div>
                            <div class="col-md-10 no-paddingl no-paddingr">
                                <h1><?php the_title(); ?></h1>
                                <?php $files = get_post_meta( get_the_ID() , 'sum_extracto', false ); $var = array_shift($files); ?>
                                <?php foreach ( $var as $info ) { echo "<div class='single-excerpt col-md-11 col-md-offset-1'><p>" . $info . "</p></div><div class='clearfix'></div>"; } ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php } else { ?>
                        <div class="single-title">
                            <h1><?php the_title(); ?></h1>
                            <?php $files = get_post_meta( get_the_ID() , 'sum_extracto', false ); $var = array_shift($files); ?>
                            <?php foreach ( $var as $info ) { echo "<div class='single-excerpt col-md-11 col-md-offset-1'><p>" . $info . "</p></div><div class='clearfix'></div>"; } ?>
                        </div>
                        <?php } ?>
                        <div class="single-social-icons col-md-3 col-xs-4 col-sm-3">
                            <div class="addthis_sharing_toolbox"></div>
                        </div>
                        <div class="col-md-9 col-xs-8 col-sm-9 author-text">
                            <?php $correo = get_the_author_meta( user_email );?>
                            <?php $twitter = get_the_author_meta( twitter );?>
                            Publicada por: <span itemprop="creator"><strong><?php the_author_posts_link(); ?></strong></span> el <span itemprop="datePublished"><?php the_date(); ?> - <?php the_time(); ?></span><span class="meta-block"><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $correo; ?>"><?php echo $correo; ?></a></span>
                            <?php if(!empty($twitter)){ ?><span class="meta-block"><i class="fa fa-twitter"></i> <a href="https://www.twitter.com/<?php echo $twitter; ?>" target="_blank">@<?php echo $twitter; ?></a></span><?php } ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 no-paddingr no-paddingl single-info" itemprop="articleBody">
                            <hr />
                            <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
                            <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
                            <?php echo '<a href="http://www.bbcmundo.com/" target="_blank"><div class="clearfix"></div><span class="bbc-bar3"><img src="'. get_bloginfo('template_url').'/images/bbc-single.gif" alt="bbc-footer" class="" /></span></a>'; ?>
                            <?php } } } ?>


                            <?php the_content(); ?>
                            <div class="clearfix"></div>
                            <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){?>
                            <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
                            <?php echo '<div class="bbc-bar2 col-md-12"><img src="'. get_bloginfo('template_url').'/images/bbc-footer.gif" alt="bbc-footer" class="" /></div>'; ?>
                            <?php } } }?>
                            <div class="clearfix"></div>
                            <div class="category-box col-md-10 col-md-offset-1 no-paddingr no-paddingl" itemprop="keywords">
                                Categoría: <?php $categories = get_the_category(); $separator = ' '; $output = ''; ?>
                                <?php if($categories){ ?>
                                <?php foreach($categories as $category) { $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator; } ?>
                                <?php echo trim($output, $separator); ?>
                                <?php } ?>
                                <?php if (has_tag()){ echo "| Claves: "; } ?>
                                <?php $posttags = get_the_tags(); ?>
                                <?php if ($posttags) { foreach($posttags as $tag) { echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a> '; } }?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12 single-share no-paddingl no-paddingr">
                            <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="3" data-colorscheme="light"></div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
