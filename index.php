<?php get_header(); ?>
<div class="container main-content">
    <div class="row">
        <div class="col-md-8 col-xs-12 col-sm-12 padding-xs contenido no-paddingl">
            <?php remove_filter('the_content', 'wpautop'); remove_filter('the_excerpt', 'wpautop'); ?>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <article itemscope itemtype="http://schema.org/NewsArticle">
                <div class="col-md-12 col-xs-12 col-sm-12 news-section1 no-paddingl no-paddingr">
                    <div class="col-md-5 col-xs-5 col-sm-5 img-section1 no-paddingl">
                        <a href="<?php the_permalink() ?>" itemprop="URL">
                            <?php $fa = "no"; $slug = ""; $terms = get_the_terms(get_the_ID(), 'content_taxonomy'); ?>
                            <?php if ( $terms != null ){ foreach ( $terms as $term ) { $slug = $term->slug; } } ?>
                            <?php if ($slug == "analisis") {$fa = "pencil";}; ?>
                            <?php if ($slug == "opinion") {$fa = "pencil";}; ?>
                            <?php if ($slug == "fotos") {$fa = "camera";}; ?>
                            <?php if ($slug == "infografia") {$fa = "area-chart";}; ?>
                            <?php if ($slug == "video") {$fa = "play";}; ?>
                            <?php if ($slug == "" or $slug == NULL) {$fa = "no";}; ?>
                            <?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'search_img', array( 'class'=>"img-responsive")); ?>
                            <?php } else { ?>
                            <?php $pic = get_post_meta(get_the_ID(), 'sum_e_url', true); $title = get_post_meta(get_the_ID(), 'sum_e_caption', true); ?>
                            <?php if (!$pic == ""){ echo '<img src="'. $pic. '" class="img-responsive" itemprop="image thumbnailURL" alt="Sumarium - '.get_the_title() .'" />'; ?>
                            <?php } else { echo '<img src="' . get_bloginfo( 'template_url' ) . '/images/no_pic.gif" class="archive-no-image"itemprop=" image thumbnailURL" alt="Sumarium - '.get_the_title() .'"/>'; ?>
                            <?php } } if ($fa != "no"){ echo '<div class="img-content-tag-small-cat"><i class="fa fa-'. $fa .'"></i></div>'; } ?>
                        </a>
                    </div>
                    <div class="col-md-7 col-xs-7 col-sm-7 info-section1">
                        <div class="col-md-12 col-xs-12 col-sm-12 title-section1"><header><a href="<?php the_permalink(); ?>" itemprop="URL"><h1 itemprop="about"><?php the_title() ?></h1></a></header></div>
                        <div class="col-md-12 col-xs-12 col-sm-12 time-section1" itemprop="datePublished"><?php echo get_the_date(); ?> - <?php echo get_the_time(); ?></div>
                        <div class="col-md-12 col-xs-12 col-sm-12 content-section1" itemprop="articleBody"><?php echo get_excerpt(250); ?></div>
                    </div>
                </div>
            </article>
            <?php endwhile; ?>
            <div class="paginator col-md-8 col-xs-8 col-sm-8 col-md-offset-4 col-xs-offset-3 col-sm-offset-3">
                <?php wp_paginate(); ?>
            </div>
            <div class="clearfix"></div>
            <?php wp_reset_postdata(); ?>
            <?php else : ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-12 visible-md visible-lg">
            <?php include(locate_template('templates/sidebar-most-recent.php'));?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
