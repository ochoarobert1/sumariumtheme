<?php get_header(); ?>
<?php 
$zon = $_GET['zon'];
$fdo_url = $_GET['fdo_url'];
$pos = 0;
$lugar = $_GET['lugar'];
$template = $_GET['template'];
	for($i = 1; $i <= $template; $i++){
		$item[$pos]->item =  $_GET['tb'. $lugar .'n'.$i];
		if ($i < '4'){
			$item[$pos]->thumbnail_url =  $_GET['url'. $lugar .'n'.$i];
		}
		$pos++;
	}
?>

<?php
//QUERY PARA TRAER LAS ZONAS ACTIVAS EN ORDEN DE POSICION
global $wpdb;
$zonas = $wpdb->get_results(
    '
            SELECT * FROM ' . $wpdb->prefix . 'pnt24_zona
            WHERE id_zona = ' . $zon . '
        '
);
global $wpdb;

$nzonas = count($zonas);
$clase = "";
$z = 1; //contador de zonas
$d = 1; // contador para cintillo
$u = 1; // contador de urgente
foreach ($zonas as $zona) {
    if ($z > 1){ $clase = "lazyload"; }
?>
<div id="zona<?php echo $z; ?>" class="zona <?php echo $clase; ?>">
    <!-- ZONAS -->
    
    <?php


                               if (!empty($fdo_url)) {
                                   $fondo = "width:100%;background-size:cover;background-image: url(" . $fdo_url . ");";
                               } else {
                                   $fondo = '';
                               }
                               $l = 1; //contador de lugares
                               // MOSTRARNDO DESTACADOS DE LA ZONA
    ?>
    <?php
                               $lugar = 'templates/page-' . $template . '-noticias.php';
                               include(locate_template($lugar));
                               if ($d == 1){
                                   include(locate_template('templates/ad-thin-fullwidth.php'));
                                   $d++;
                               }
    ?>

    <div class="clearfix"></div>

    <?php
	   //HACER EL QUERY PARA LOS BLOQUES
	   global $wpdb;
	   $bloques = $wpdb->get_results(
		   '
                SELECT * FROM ' . $wpdb->prefix . 'pnt24_lugar
                WHERE id_zona = ' . $zona->id_zona . '
                AND TIPO = 2
                AND activo = 1
                ORDER BY id_zona
                LIMIT 1
            '
            );
		   // MOSTRARNDO BLOQUE DE LA ZONA
		   foreach ($bloques as $bloque) {
    ?>
    <section id="skeleton-loop">
        <div class="skeleton-loop">
            <div class="container">
                <div class="row">


                    <div class="col-md-8 nosidepadding">

                    <?php global $wpdb;
                         $titulo = $wpdb->get_results(" SELECT * FROM " . $wpdb->prefix . "pnt24_lugar WHERE id_zona = " . $zona->id_zona . " AND tipo = 2 AND activo = 1");
                         if (!empty($titulo[0]->fondo_url) || !empty($titulo[0]->texto)) {
                     ?>
                            <div class="col-md-12 nosidepadding block-front-title">
                                <?php if (!empty($titulo[0]->texto)) {?>
                                        <div class="col-md-3 col-xs-3 col-sm-3 no-paddingl no-paddingr separador-outside"></div>
                                        <div class="col-md-6 col-xs-6 col-sm-6 separador-inside no-paddingl no-paddingr" style="background:<?php echo $titulo[0]->fond_color; ?>;">
                                           <div class="col-md-12 separador-text" style="background:<?php echo $titulo[0]->fond_color; ?>; border: 1px solid <?php echo $titulo[0]->text_color;?>; color: <?php echo $titulo[0]->text_color;?>;">
                                               <?php echo $titulo[0]->texto;?>
                                           </div>
                                        </div>
                                        <div class="col-md-3 col-xs-3 col-sm-3 no-paddingl no-paddingr separador-outside"></div>
                                <?php } else { ?>
                                <?php /*if (!empty($titulo[0]->fondo_url)) {*/ ?>
                                    <img src="<?php echo $titulo[0]->fondo_url; ?>" alt="Sumarium" class="img-responsive" />
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>

                        <?php } ?>

                    <?php
                        $elements = $wpdb->get_results(" SELECT * FROM ".$wpdb->prefix."pnt24_pending left join ".$wpdb->prefix."pnt24_into_pending using(id_pending) left join ".$wpdb->prefix."terms using(term_id) where id_lugar = ".$bloque->id_lugar);
                        if ($elements[0]->active == '1'){
                    ?>
                            <div class="col-md-12 nosidepadding block-front-title block-tags-container">
                                <ul class="block-tags-content">
                                    <?php foreach($elements as $term){ ?>
                                    <li class="block-item"><a href="<?php echo home_url('/');?>tag/<?php echo $term->slug; ?>"><div class="block-text"><?php echo $term->name;?></div></a></li>
                                    <?php } ?>
                                </ul>
                            </div>

                        <?php } ?>

                        <div class="col-md-7 no-paddingl">
                             <?php include(locate_template('templates/five-cols-front-item.php')); ?>
                        </div>
                        <div class="three-cols-main-container col-md-5 ">
                            <?php include(locate_template('templates/three-cols-front-item.php')); ?>
                        </div>

                    </div>

                    <div class="col-md-4 nosidepadding">
                        <div class="sidebar-section-loop">
                            <?php
                               include(locate_template('templates/sidebar-front-page.php'));
                               //get_template_part('templates/sidebar-front-page');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
                           }//FIN BLOQUE
    ?>
    <?php
                           
                           if ($zona->pics == '1') {
                               get_template_part('templates/featured-pics');
                           }
						   
						   if ($zona->voces == '1') {
                               get_template_part('templates/featured-opinions');
                           }
    ?>
    <?php
                           //cargar el separador
                           if ($zona->separador > 0) {
    ?>
    <div class="container zoneads">
        <div class="row">
            <?php the_ad_group($zona->separador); ?>
        </div>
    </div>
    <?php }
    ?>
</div>
<?php
                           $z++;
                          }; //FIN ZONAS
?>
<?php get_footer(); ?>
