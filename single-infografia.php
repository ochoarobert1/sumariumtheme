<?php get_header(); ?>
<?php the_post(); ?>
<?php $termeditorial = 0; $categories = get_the_category(); if ( ! empty( $categories ) && ! is_wp_error( $categories ) ){ foreach ($categories as $categoria){ $cat = $categoria->name; } }?>
<?php $formato = get_post_meta( get_the_ID() , 'sum_single_format', true ); ?>
<?php if ($formato == 'parallax') {
    get_template_part('template-parallax-single');
} else {
    get_template_part('template-normal-single');
}
?>
<style type="text/css">
    <?php echo get_post_meta(get_the_ID(), 'sum_customcss', true); ?>
</style>
<script type="text/javascript">
    $('.iframe-info').nicescroll();
    <?php echo get_post_meta(get_the_ID(), 'sum_customjs', true); ?>
</script>
<?php get_footer(); ?>
