<?php get_header(); ?>
<div class="container-fluid">
    <script type="text/javascript">
        var ruta = '<?php bloginfo('template_url'); ?>';
    </script>
    <?php
//Consultas a los padres
//recupero la URL
/*$url = $_SERVER['REQUEST_URI'];*/
//echo '<br />'.$url.'<br />';

$tagname = get_query_var('tag');
$tags = $tagname;
$tagname = single_tag_title("", false);
/*echo $tagname;
//var_dump($paginas);
//Tengo toda las paginas que funcionan como categorias
foreach( $posttags as $pagina ){
    //Comparo la lista de categorias con la de la url, si la consigue fino, sino muestro error
    $verify = strstr($url, $pagina->slug);

    if(!empty($verify)){
        $tags = $pagina->slug;
        $tagname = $pagina->name;
        $cat = $verify;
        break;
    } else {
        $cat = 'Hay algo mal aqui!';
    }
}*/
    ?>
    <div class="row">
        <div class="col-md-12 col-xs-12 col-sm-12 title-tag-container ">
            <?php echo $tagname; ?>
        </div>
        <div class="responsive-tag-container">
            <div id="tags-bar" class="tags-main-container hidden-xs col-md-2 col-sm-4 no-paddingl no-paddingr">
                <?php include(locate_template('templates/sidebar-related-tags.php'));?>
            </div>
            <div id="tags-bar2" class="tags-mini-container2 visible-xs col-xs-12 no-paddingl no-paddingr">
                <?php include(locate_template('templates/sidebar-related-tags-xs.php'));?>
            </div>
            <div class="bar-xs-tap">
                <div id="tags-xs-wrapper" class="button-xs-tap visible-xs">
                    <button class="btn btn-sm btn-danger">tap para menu</button>
                </div>
            </div>
            <div id="content-bar" class="col-md-10 col-xs-12 col-sm-8 contenido-tags ">
                <?php
// The Loop
$y = 1;
query_posts( array ('posts_per_page' => 1, 'tag' => $tags) );
if ( have_posts() ) :
while ( have_posts() ) : the_post();
                ?>

                <div id="tag_frame" class="col-md-12 col-xs-12 col-sm-12 news-section1 main-content no-paddingl no-paddingr">
                    <article>
                        <div class="col-md-12 the-post-single no-paddingl no-paddingr">
                            <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if (!empty($terms)) {?>
                            <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
                            <?php echo '<div class="bbc-bar col-md-12"><img src="'. get_bloginfo('template_url').'/images/bbc-bar.png" alt="bbc-bar" class="" /></div>'; ?>
                            <?php } } }?>
                            <div class="col-md-12">
                                <div class="single-title">
                                    <h1><?php the_title(); ?></h1>
                                    <?php $files = get_post_meta( get_the_ID() , 'sum_extracto', false ); $var = array_shift($files); ?>
                                    <?php foreach ( $var as $info ) { echo "<div class='single-excerpt col-md-11 col-md-offset-1'><p>" . $info . "</p></div><div class='clearfix'></div>"; } ?>
                                </div>
                                <?php
$pic = get_post_meta(get_the_ID(), 'sum_e_url', true);
$title = get_post_meta(get_the_ID(), 'sum_e_caption', true);
if (!$pic == ""){
    echo '<div class="single-img"><img src="'. $pic. '" class="img-responsive"/>'; echo '<span class="single-img-text">' . $title . '</span>'; echo '</div>';
}
                                ?>
                                <div class="col-md-2 col-xs-4 col-sm-3">
                                    <div class="addthis_sharing_toolbox"></div>
                                </div>
                                <div class="col-md-10 col-xs-8 col-sm-9 author-text">
                                    <?php $correo = get_the_author_meta( user_email, $userID );?>
                                    <?php $twitter = get_the_author_meta( twitter, $userID );?>
                                    Publicada por: <span itemprop="creator"><strong><?php the_author(); ?></strong></span> el <span itemprop="datePublished"><?php the_date(); ?> - <?php the_time(); ?></span><span class="meta-block"><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $correo; ?>"><?php echo $correo; ?></a></span>
                                    <?php if(!empty($twitter)){ ?><span class="meta-block"><i class="fa fa-twitter"></i> <a href="https://www.twitter.com/<?php echo $twitter; ?>" target="_blank">@<?php echo $twitter; ?></a></span><?php } ?>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-12 no-paddingr no-paddingl single-info">
                                    <hr />
                                    <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if (!empty($terms)) {?>
                                    <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
                                    <?php echo '<a href="http://www.bbcmundo.com/" target="_blank"><div class="clearfix"></div><span class="bbc-bar3"><img src="'. get_bloginfo('template_url').'/images/bbc-single.gif" alt="bbc-footer" class="" /></span></a>'; ?>
                                    <?php } } }?>
                                    <?php the_content(); ?>
                                    <div class="clearfix"></div>
                                    <?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if (!empty($terms)) {?>
                                    <?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
                                    <?php echo '<div class="bbc-bar2 col-md-12"><img src="'. get_bloginfo('template_url').'/images/bbc-footer.gif" alt="bbc-footer" class="" /></div>'; ?>
                                    <?php } } }?>
                                    <div class="category-box col-md-10 col-md-offset-1 no-paddingr no-paddingl" itemprop="keywords">
                                        Categoría: <?php $categories = get_the_category(); $separator = ' '; $output = ''; ?>
                                        <?php if($categories){ ?>
                                        <?php foreach($categories as $category) { $output .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator; } ?>
                                        <?php echo trim($output, $separator); ?>
                                        <?php } ?>
                                        <?php if (has_tag()){ echo "| Claves: "; } ?>
                                        <?php $posttags = get_the_tags(); ?>
                                        <?php if ($posttags) { foreach($posttags as $tag) { echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a> '; } }?>
                                    </div>
                                    <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="3" data-colorscheme="light"></div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <?php $y++; endwhile; ?>
                <div class="clearfix"></div>
                <?php wp_reset_postdata(); ?>
                <?php else : ?>
                <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php $terms = get_the_terms( $post->ID , 'content_taxonomy' ); if (!empty($terms)) {?>
<?php foreach ($terms as $term) { if ($term->slug == 'bbc'){ ?>
<!-- start tracking code -->

<noscript><img src="http://sa.bbc.com/b/ss/bbcwglobalprod,bbcmundo/1/H.26.2--NS/0?c45=mundo&c25=sumarium.com" height="1" width="1" border="0" alt="" /></noscript><script language="JavaScript" type="text/javascript" src="http://static.bbci.co.uk/ws/js/vendor/site_catalyst/s_code_bbcws.js"></script>
<script>
    s_bbcws.prop25="sumarium.com";
    s_bbcws.prop45="mundo";
    s_bbcws.t();
</script>
<!-- end tracking code -->
<?php } } }?>
<?php get_footer(); ?>
