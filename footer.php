<footer>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-xs-12 col-sm-12 footer-fl no-paddingl no-paddingr">
                    <div class="col-md-5 col-xs-12 col-sm-5 no-paddingr">
                        <div class="logo-footer">
                            <a href="<?php bloginfo('url'); ?>">
                                <img src="<?php bloginfo('template_url');?>/images/logo.png" alt="Sumarium Logo">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6 no-paddingl no-paddingr">
                        <form class="form-inline footer-inline" role="form" method="get" action='<?php echo home_url('/'); ?>'>
                            <input class="search" type="text" name="s" placeholder="¿No encontró la noticia que deseaba?, ingrese su búsqueda aquí">
                            <button type="submit"></button>
                        </form>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12 col-sm-12 footer-fl no-paddingl no-paddingr">
                    <h2 class="text-center">"No se puede ser imparcial entre la verdad y la mentira"</h2>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12 col-sm-12 footer-sl no-paddingl no-paddingr">

                    <div class="clearfix"></div>
                    <div class="col-md-3 col-xs-12 col-sm-6 no-paddingl no-paddingr">
                        <a href="<?php echo home_url('/'); ?>somos-sumarium/" class="footer-link"><p>El Periodismo que Hacemos</p></a>
                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-6 no-paddingl no-paddingr">
                        <a href="<?php echo home_url('/'); ?>somos-sumarium/" class="footer-link"><p>Somos Sumarium</p></a>
                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-6 no-paddingl no-paddingr">
                        <a href="<?php echo home_url('/'); ?>somos-sumarium/" class="footer-link"><p>Principios que no quebramos</p></a>
                    </div>
                    <div class="col-md-3 col-xs-12 col-sm-6 no-paddingl no-paddingr text-right">
                        <a href="<?php echo home_url('/'); ?>somos-sumarium/" class="footer-link"><p>Habla con Nosotros</p></a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12 col-xs-12 col-sm-12 no-paddingl no-paddingr footer-copy">
                    <div class="col-md-4 col-xs-12 col-sm-4 no-paddingl no-paddingr">
                        <p class="copy">&copy;2015<span class="divider">|</span>www.sumarium.com</p>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-8 no-paddingl no-paddingr text-right">
                        <p class="copy">Catalunya Media Group, C.A. Rif. J-40391716-7.<span class="divider">|</span>Contáctenos vía <a class="mainmail" href="mailto:sumariumcom@gmail.com">sumariumcom@gmail.com</a></p>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <form id="fecha-form" name="fecha-form" action="<?php echo home_url('categoria/opinion')?>" method="POST">
        <input type="hidden" name="fecha-hidden" id="fecha-hidden" value=""  />
    </form>
    <!-- SCRIPTS -->
    <script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <!--    <script src="<?php bloginfo('template_url'); ?>/js/modernizr.min.js"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/smooth-scroll.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.sticky.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/functions.js"></script>
</footer>
<?php wp_footer(); ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-62581391-3', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
