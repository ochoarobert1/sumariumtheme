<?php get_header(); ?>
<?php
/* REMOVE AUTOP FROM CONTENT - EXCERPT */
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');
date_default_timezone_set ('America/Caracas');
$dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sábado");
$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $variable = $_POST['fecha-hidden'];
    $array = explode("/", $variable);
    $fecha = $array[0];
    $fecha .= '-';
    $fecha .= $array[1];
    $fecha .= '-';
    $fecha .= $array[2];
}


//Salida: Viernes 24 de Febrero del 2012
?>
<div class="container main-content">
    <div class="row">
        <div class="col-md-8 col-xs-8 col-sm-8">
            <h1 class="opinion-title-h2">Opinión</h1>
        </div>
        <div class="col-md-4 col-xs-4 col-sm-4">
            <h3 class="text-right">
                <?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    echo $dias[date('w', strtotime($fecha))].", ".date('d', strtotime($fecha))." de ".$meses[date('n', strtotime($fecha))-1]. " del ".date('Y', strtotime($fecha)) ;

} else {
    echo $dias[date('w')].", ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

}
                ?>
            </h3>
        </div>
        <div class="clearfix"></div>
        <hr class="title-separador">


        <div class="col-md-8 col-xs-12 col-sm-12 contenido no-paddingl">
            <div class="col-md-12 no-paddingl no-paddingr">
                <?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fechats = explode('-',$fecha);
    $today['year'] = $fechats[0];
    $today['mon'] = $fechats[1];
    $today['mday'] = $fechats[2];
} else {
    $today = getdate();
}
$args=array(
    'date_query' => array(
        array(
            'year'  => $today['year'],
            'month' => $today['mon'],
            'day'   => $today['mday'],
        ),
    ),
    'content_taxonomy' => 'editorial1',
    'posts_per_page' => 1
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <article>
                    <div class="col-md-12 news-section1 no-paddingl no-paddingr">
                        <div class="article-opinion-title">Editorial</div>
                        <div class="col-md-12 title-section1"><header><a href="<?php the_permalink(); ?>"><h1><?php the_title() ?></h1></a></header></div>
                        <div class="col-md-12 featured-info-section">
                            <div class="col-md-12 time-section1"><?php echo get_the_date(); ?> - <?php echo get_the_time(); ?> - Por: <strong><?php the_author(); ?></strong></div>
                            <div class="col-md-12 featured-content-section1"><?php echo get_excerpt(600); ?></div>
                        </div>

                    </div>
                </article>
                <?php endwhile; ?>
                <?php } ?>
                <?php wp_reset_postdata(); ?>
                <?php wp_reset_query(); ?>
            </div>
            <div class="col-md-12 no-paddingl no-paddingr">
                <?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fechats = explode('-',$fecha);
    $today['year'] = $fechats[0];
    $today['mon'] = $fechats[1];
    $today['mday'] = $fechats[2];
} else {
    $today = getdate();
}
$args=array(
    'date_query' => array(
        array(
            'year'  => $today['year'],
            'month' => $today['mon'],
            'day'   => $today['mday'],
        ),
    ),
    'content_taxonomy' => 'editorial2',
    'posts_per_page' => 1
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) {
    while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <article>
                    <div class="col-md-12 news-section1 no-paddingl no-paddingr">
                        <div class="article-opinion-title">Editorial</div>
                        <div class="col-md-12 title-section1"><header><a href="<?php the_permalink(); ?>"><h1><?php the_title() ?></h1></a></header></div>
                        <div class="col-md-12 featured-info-section">
                            <div class="col-md-12 time-section1"><?php echo get_the_date(); ?> - <?php echo get_the_time(); ?> - Por: <strong><?php the_author(); ?></strong></div>
                            <div class="col-md-12 featured-content-section1"><?php echo get_excerpt(600); ?></div>
                        </div>

                    </div>
                </article>
                <?php endwhile; ?>
                <?php } ?>
                <?php wp_reset_postdata(); ?>
                <?php wp_reset_query(); ?>
            </div>
            <?php
// The Loop
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fechats = explode('-',$fecha);
    $today['year'] = $fechats[0];
    $today['mon'] = $fechats[1];
    $today['mday'] = $fechats[2];
} else {
    $today = getdate();
}
$args=array(
    'date_query' => array(
        array(
            'year'  => $today['year'],
            'month' => $today['mon'],
            'day'   => $today['mday'],
        ),
    ),
    'content_taxonomy' => 'breve-editorial',
    'posts_per_page' => 3
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) { ?>
            <div class="article-opinion-title visible-md visible-sm visible-lg">Breves Editoriales</div>
            <div class="breve-editorial-container col-md-12 no-padddingl no-paddingr">
                <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
                <article>
                    <div class="breve-editorial-section col-md-4 visible-md visible-sm visible-lg col-sm-4 no-paddingl no-paddingr">
                        <div class="col-md-12 info-section2">
                            <div class="col-md-12 title-section1"><header><a href="<?php the_permalink(); ?>"><h1><?php the_title() ?></h1></a></header></div>
                            <div class="col-md-12 time-section1"><?php echo get_the_date(); ?> - <?php echo get_the_time(); ?></div>
                            <div class="col-md-12 time-section1">Por: <strong><?php the_author(); ?></strong></div>
                            <div class="col-md-12 content-section1"><?php echo get_excerpt(200); ?></div>
                        </div>
                    </div>
                </article>
                <?php endwhile; ?>
                <div class="clearfix"></div>
                <?php wp_reset_postdata(); ?>
            </div>
            <?php } ?>

            <div class="clearfix"></div>
            <?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $fechats = explode('-',$fecha);
    $today['year'] = $fechats[0];
    $today['mon'] = $fechats[1];
    $today['mday'] = $fechats[2];
} else {
    $today = getdate();
}
$args=array(
    'date_query' => array(
        array(
            'year'  => $today['year'],
            'month' => $today['mon'],
            'day'   => $today['mday'],
        ),
    ),
    'content_taxonomy' => 'confidencias',
    'posts_per_page' => 3
);
$my_query = null;
$my_query = new WP_Query($args);
if( $my_query->have_posts() ) { ?>
    <div class="article-opinion-title">Confidencias</div>
    <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
            <article>
                <div class="col-md-12 confidencias-section no-paddingl no-paddingr">
                    <div class="col-md-12 info-section1">
                        <div class="col-md-12 title-section1"><header><a href="<?php the_permalink(); ?>"><h1><?php the_title() ?></h1></a></header></div>
                        <div class="col-md-12 time-section1"><?php echo get_the_date(); ?> - <?php echo get_the_time(); ?> - Por: <strong><?php the_author(); ?></strong></div>
                        <div class="col-md-12 content-section1"><?php echo get_excerpt(200); ?></div>
                    </div>
                </div>
            </article>
            <?php endwhile; ?>
            <div class="clearfix"></div>
            <?php wp_reset_postdata(); ?>
            <?php } ?>
        </div>
        <div class="col-md-3 col-xs-12 col-sm-12">
            <div class="col-md-12 col-xs-12 col-sm-12 no-paddingl no-paddingr marginb20">
                <?php /* LLAMADO DE ESTA MANERA PARA QUE VARIABLE PASE AL TEMPLATE */ ?>
                <?php include(locate_template('templates/sidebar-opinion.php'));?>
                <div class="clearfix"></div>
                <?php include(locate_template('templates/sidebar-opinion-section.php'));?>
                <div class="clearfix"></div>
                <?php include(locate_template('templates/sidebar-featured-opinions.php'));?>
                <div class="clearfix visible-md visible-lg"></div>
                <?php include(locate_template('templates/sidebar-calendar.php'));?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php
get_footer('opinion');
?>
