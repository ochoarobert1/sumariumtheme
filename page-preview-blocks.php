<?php get_header(); ?>
<?php
$zon = $_GET['zon'];
$lugar = $_GET['lugar'];
$titulo = $_GET['titulo'];
$color_fondo = $_GET['color_fondo'];
$color_texto = $_GET['color_texto'];
$fondo_url = $_GET['fondo_url'];
$can_blo1 = $_GET['can_blo1'];
$can_blo2 = $_GET['can_blo2'];
$bloque = $lugar;

	
	for($i = 1; $i <= $can_blo1; $i++){
		$posicion_1 = $_GET["posicionb".$lugar."c1n".$i];
		$itemcol1[$posicion_1]->item = $_GET["tb".$lugar."c1n".$i];
		$itemcol1[$posicion_1]->thumbnail_url = $_GET["url".$lugar."c1n".$i];
	}
	for($i = 1; $i <= $can_blo2; $i++){
		$posicion_2 = $_GET["posicionb".$lugar."c2n".$i];
		$itemcol2[$posicion_2]->item = $_GET["tb".$lugar."c2n".$i];
		$itemcol2[$posicion_2]->thumbnail_url = $_GET["url".$lugar."c2n".$i];
	}
?>

<?php
//QUERY PARA TRAER LAS ZONAS ACTIVAS EN ORDEN DE POSICION
global $wpdb;
$zonas = $wpdb->get_results(
    '
            SELECT * FROM ' . $wpdb->prefix . 'pnt24_zona
            WHERE id_zona = ' . $zon . '
        '
);
global $wpdb;

$nzonas = count($zonas);
$clase = "";
$z = 1; //contador de zonas
$d = 1; // contador para cintillo
$u = 1; // contador de urgente
foreach ($zonas as $zona) {
    if ($z > 1){ $clase = "lazyload"; }
?>
<div id="zona<?php echo $z; ?>" class="zona <?php echo $clase; ?>">
    <!-- ZONAS -->
    <?php
                           //HACER EL QUERY PARA LOS DESTACADOS
                           global $wpdb;
                           $destacados = $wpdb->get_results(
                               '
                SELECT * FROM ' . $wpdb->prefix . 'pnt24_lugar
                WHERE id_zona = ' . $zona->id_zona . '
                AND TIPO = 1
                AND activo = 1
                ORDER BY id_zona
                LIMIT 1
            '
                           );

                           foreach ($destacados as $destacado) {

                               //hacer la consulta para los posts
                               global $wpdb;
                               $item = $wpdb->get_results(
                                   '
                    SELECT * FROM ' . $wpdb->prefix . 'pnt24_item
                    WHERE id_lugar = ' . $destacado->id_lugar . '
                '
                               );


                               if (!empty($destacado->fondo_url)) {
                                   $fondo = "width:100%;background-size:cover;background-image: url(" . $destacado->fondo_url . ");";
                               } else {
                                   $fondo = '';
                               }
                               $l = 1; //contador de lugares
                               // MOSTRARNDO DESTACADOS DE LA ZONA
    ?>
    <?php
                               $lugar = 'templates/page-' . $destacados[0]->plantilla . '-noticias.php';
                               include(locate_template($lugar));
                               if ($d == 1){
                                   include(locate_template('templates/ad-thin-fullwidth.php'));
                                   $d++;
                               }
    ?>
    <?php
                           } //FIN DESTACADO
    ?>
    <div class="clearfix"></div>

    
    <section id="skeleton-loop">
        <div class="skeleton-loop">
            <div class="container">
                <div class="row">


                    <div class="col-md-8 nosidepadding">

                    <?php global $wpdb;
                         
                         if (!empty($fondo_url) || !empty($titulo)) {
                     ?>
                            <div class="col-md-12 nosidepadding block-front-title">
                                <?php if (!empty($titulo)) {?>
                                        <div class="col-md-3 col-xs-3 col-sm-3 no-paddingl no-paddingr separador-outside"></div>
                                        <div class="col-md-6 col-xs-6 col-sm-6 separador-inside no-paddingl no-paddingr" style="background:<?php echo $color_fondo; ?>;">
                                           <div class="col-md-12 separador-text" style="background:<?php echo $color_fondo; ?>; border: 1px solid <?php echo $color_texto;?>; color: <?php echo $color_texto;?>;">
                                               <?php echo $titulo;?>
                                           </div>
                                        </div>
                                        <div class="col-md-3 col-xs-3 col-sm-3 no-paddingl no-paddingr separador-outside"></div>
                                <?php } else { ?>
                                    <img src="<?php echo $fondo_url; ?>" alt="Sumarium" class="img-responsive" />
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                        <?php } ?>


                    <?php
                        $elements = $wpdb->get_results(" SELECT * FROM ".$wpdb->prefix."pnt24_pending left join ".$wpdb->prefix."pnt24_into_pending using(id_pending) left join ".$wpdb->prefix."terms using(term_id) where id_lugar = ".$bloque);
                        if ($elements[0]->active == '1'){
                    ?>
                            <div class="col-md-12 nosidepadding block-front-title block-tags-container">
                                <ul class="block-tags-content">
                                    <?php foreach($elements as $term){ ?>
                                    <li class="block-item"><a href="<?php echo home_url('/');?>tag/<?php echo $term->slug; ?>"><div class="block-text"><?php echo $term->name;?></div></a></li>
                                    <?php } ?>
                                </ul>
                            </div>

                        <?php } ?>

                        <div class="col-md-7 no-paddingl">
                             <?php include(locate_template('templates/cols-five-preview.php')); ?>
                        </div>
                        <div class="three-cols-main-container col-md-5 ">
                            <?php include(locate_template('templates/cols-three-preview.php')); ?>
                        </div>

                    </div>

                    <div class="col-md-4 nosidepadding">
                        <div class="sidebar-section-loop">
							
							<div class="front-ads-container">
	
								<?php
									$sidebar = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "pnt24_sidebars left join " . $wpdb->prefix . "pnt24_into_sidebar using(id_sidebar) where active <> 0 AND id_lugar = " . $bloque . " ORDER BY posicion ASC" );	
								?>
								
								<?php foreach ($sidebar as $side) { ?>
									<div class="col-md-12 ads-single">
										<div class="sidebar-front-page-news">
										<?php 
										//variable a utilizar para delimitar la cantidad de noticias a montrar por cada sidebar
										$cantidad = $side->cantidad;
										//variable con el nombre del tag
										$tag = $side->tag;
										//variable con la url para la imagen de fondo de editor pick
										$url_fondo = $side->url_fondo;
										//variable para la busqueda del tag
										//$slug = $side->slug;
										?>
										
										  <?php if ($side->tipo == 1) { ?>
											  <div class="col-md-12 ads-single">
												<?php include(locate_template('templates/sidebar-most-recent.php'));?>
											  </div>
										  <?php } ?>
										  
										  <?php if ($side->tipo == 2) { ?>
											  <div class="col-md-12 ads-single">
													<?php include(locate_template('templates/sidebar-most-view.php'));?>
											  </div>
										  <?php } ?>
										  
										  <?php if ($side->tipo == 3) { ?>
											  <div class="col-md-12 ads-single">
												<?php include(locate_template('templates/sidebar-ministories.php'));?>
											  </div>
										  <?php } ?>
										  
										  <?php if ($side->tipo == 4) { ?>
											  <div class="col-md-12 ads-single">
												<?php include(locate_template('templates/sidebar-editorspick.php'));?>
											  </div>
										  <?php } ?>
										  
										</div>
									</div>
							  <?php } ?>
							  
							</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
                           
	if ($zona->pics == '1') {
		get_template_part('templates/featured-pics');
	}
	   
	if ($zona->voces == '1') {
	   get_template_part('templates/featured-opinions');
	}
    ?>
    <?php
    //cargar el separador
    if ($zona->separador > 0) {
    ?>
		<div class="container zoneads">
			<div class="row">
				<?php the_ad_group($zona->separador); ?>
			</div>
		</div>
    <?php } ?>
</div>
<?php
                           $z++;
                          }; //FIN ZONAS
?>
<?php get_footer(); ?>
