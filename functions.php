<?php

/* --------------------------------------------------------------
    ADD WORDPRESS THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain('sumarium', get_template_directory() . '/languages');
$markup = array('search-form', 'comment-form', 'comment-list',);
add_theme_support('html5', $markup);
add_theme_support('post-thumbnails');
add_theme_support('menus');
function custom_theme_features()  {
    add_editor_style( get_bloginfo('template_url') . '/css/editor-style.css' );
}
add_action( 'after_setup_theme', 'custom_theme_features' );

/** CUSTOM LOGIN STYLE **/
add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
    echo '
    <style type="text/css">
        body{
            background-image:url(' . get_bloginfo('template_directory') . '/images/login-bg.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        h1 {
            background-image:url(' . get_bloginfo('template_directory') . '/images/logo.png) !important;
            background-repeat: no-repeat;
            background-position: center;
        }
        a { background-image:none !important;}
        .login form{-webkit-border-radius: 0px; border-radius: 0px; background-color: rgba(255,255,255,0.7);}
        .login label{color: black; font-weight:bold;}
    </style>
    ';
}

/** META TWITTER PARA USUARIO EN EL WORDPRESS **/
function custom_contact_fields( $contact_meta ) {
    if ( !isset( $contact_meta['twitter'] ) ) {
        $contact_meta['twitter'] = 'Usuario de Twitter';
    }
    return $contact_meta;
}
add_filter( 'user_contactmethods', 'custom_contact_fields' );

/** ORDENAR BUSQUEDA POR FECHA **/
function my_search_query( $query ) {
    // not an admin page and is the main query
    if ( !is_admin() && $query->is_main_query() ) {
        if ( is_search() ) { $query->set( 'orderby', 'date' ); }
    }
}
add_action( 'pre_get_posts', 'my_search_query' );

/** CAPTIONS PARA POST THUMBNAIL  **/
function the_post_thumbnail_caption() {
    global $post;
    $thumbnail_id    = get_post_thumbnail_id($post->ID);
    $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));
    if ($thumbnail_image && isset($thumbnail_image[0])) {
        echo '<span class="single-img-text">Foto: '.$thumbnail_image[0]->post_excerpt.'</span>';
    }
}

/* --------------------------------------------------------------
    CUSTOM METABOX REGISTRATION
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'sum_register_meta_boxes' );
function sum_register_meta_boxes( $meta_boxes )
{
    $prefix = 'sum_';
    $meta_boxes[] = array(
        'title'    => 'Detalle - Información',
        'pages'    => array( 'post' ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields' => array(
            array(
                'name'  => 'Extracto',
                'desc'  => 'Extracto de noticia que va situado debajo del titulo principal',
                'id'    => $prefix . 'extracto',
                'type'  => 'textarea',
                'cols'  => 35,
                'rows'  => 1,
                'clone' => true,
            ),
        )
    );
    $meta_boxes[] = array(
        'title'    => 'Imagen Banner Destacado',
        'pages'    => array( 'post' ),
        'context'  => 'side',
        'priority' => 'high',
        'fields' => array(
            array(
                'name'  => 'URL',
                'desc'  => 'Formato: Http://www.link.com/pic.jpg </br> Tamaño Normal: 1138 de Ancho (Altura Variable) </br> Tamaño Parallax: 1650 ancho x 600 alto (Ancho Variable)',
                'id'    => $prefix . 'e_url',
                'type'  => 'text'
            ),
            array(
                'name'  => 'Leyenda',
                'desc'  => 'Formato: Fuente: XXX - Este es un Caption',
                'id'    => $prefix . 'e_caption',
                'type'  => 'text'
            ),
        )
    );
    $meta_boxes[] = array(
        'title'    => 'Formato de Notas',
        'pages'    => array( 'post' ),
        'context'  => 'side',
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'Tipos de nota:',
                'id'   => $prefix . 'single_format',
                'type' => 'select',
                'options'     => array(
                    'normal' => __( 'Formato Estandar', 'normal' ),
                    'parallax' => __( 'Formato en Parallax', 'parallax' )
                )
            ),
        )
    );
     $meta_boxes[] = array(
        'title'    => 'Código para Infografía',
        'pages'    => array( 'infografia' ),
        'priority' => 'high',
        'fields' => array(
            array(
                'name' => 'Custom CSS:',
                'desc'  => 'Estilo custom para infografía <br /> Nota: solo funcionará en esta infografia. <br /> Agregar sin etiquetas <style></style>',
                'id'   => $prefix . 'customcss',
                'type' => 'textarea'
            ),
            array(
                'name' => 'Custom JS:',
                'desc'  => 'Script custom para infografía <br /> Nota: solo funcionará en esta infografia. <br /> Agregar sin etiquetas <script></script>',
                'id'   => $prefix . 'customjs',
                'type' => 'textarea'
            ),
        )
    );
    return $meta_boxes;
}

/* --------------------------------------------------------------
    CUSTOM POST TYPE
-------------------------------------------------------------- */

function infografia() {

    $labels = array(
        'name'                => _x( 'Infografías', 'Post Type General Name', 'sumarium' ),
        'singular_name'       => _x( 'Infografía', 'Post Type Singular Name', 'sumarium' ),
        'menu_name'           => __( 'Infografía', 'sumarium' ),
        'name_admin_bar'      => __( 'Infografía', 'sumarium' ),
        'parent_item_colon'   => __( 'Infografía Padre:', 'sumarium' ),
        'all_items'           => __( 'Todas las Infografías', 'sumarium' ),
        'add_new_item'        => __( 'Agregar Infografía', 'sumarium' ),
        'add_new'             => __( 'Agregar Infografía', 'sumarium' ),
        'new_item'            => __( 'Nueva Infografía', 'sumarium' ),
        'edit_item'           => __( 'Editar Infografía', 'sumarium' ),
        'update_item'         => __( 'Actualizar Infografía', 'sumarium' ),
        'view_item'           => __( 'Ver Infografía', 'sumarium' ),
        'search_items'        => __( 'Buscar Infografía', 'sumarium' ),
        'not_found'           => __( 'Infografía no encontrada', 'sumarium' ),
        'not_found_in_trash'  => __( 'Infografía no encontrada', 'sumarium' ),
    );
    $args = array(
        'label'               => __( 'Infografía', 'sumarium' ),
        'description'         => __( 'Infografias generadas', 'sumarium' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 20,
        'menu_icon'           => 'dashicons-media-interactive',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
    );
    register_post_type( 'infografia', $args );

}
add_action( 'init', 'infografia', 0 );

/* --------------------------------------------------------------
    CUSTOM TAXONOMIES
-------------------------------------------------------------- */
/* TIPOS DE CONTENIDO */

function content_type_taxonomy() {
    $labels = array(
        'name'                       => _x( 'Tipos de Contenido', 'Taxonomy General Name', 'sumarium' ),
        'singular_name'              => _x( 'Tipo de Contenido', 'Taxonomy Singular Name', 'sumarium' ),
        'menu_name'                  => __( 'Tipos de contenido', 'sumarium' ),
        'all_items'                  => __( 'Todos los Tipos', 'sumarium' ),
        'parent_item'                => __( 'Tipo Contenido Padre', 'sumarium' ),
        'parent_item_colon'          => __( 'Tipo Contenido Padre:', 'sumarium' ),
        'new_item_name'              => __( 'Nuevo tipo de Contenido', 'sumarium' ),
        'add_new_item'               => __( 'Agregar nuevo', 'sumarium' ),
        'edit_item'                  => __( 'Editar', 'sumarium' ),
        'update_item'                => __( 'Actualizar', 'sumarium' ),
        'separate_items_with_commas' => __( 'Separar tipos por comas', 'sumarium' ),
        'search_items'               => __( 'Buscar', 'sumarium' ),
        'add_or_remove_items'        => __( 'Agregar o Remover', 'sumarium' ),
        'choose_from_most_used'      => __( 'Escoger de los mas usados', 'sumarium' ),
        'not_found'                  => __( 'No encontrado', 'sumarium' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'content_taxonomy', array( 'post' ), $args );
}
add_action( 'init', 'content_type_taxonomy', 0 );

/* --------------------------------------------------------------
    CUSTOM IMAGE RESIZE
-------------------------------------------------------------- */

if (function_exists('add_image_size')) {
    add_image_size('search_img', 9999, 220, false);
    add_image_size('sidebar_img', 60, 60, true);
    add_image_size('single_img', 9999, 800, false);
    add_image_size('archive_img', 9999, 350, false);
}

/* --------------------------------------------------------------
    CUSTOM SHORTCODES
-------------------------------------------------------------- */

/* SHORTCODE PARA LA GALERIA DE IMAGENES EN FOTOS */
function adding_custom_button($atts, $content = "") {
    return '<div class="posted-img">' . $content . '</div>';
}
add_shortcode('image-frame', 'adding_custom_button');
function my_add_mce_button() {
    // check user permissions
    if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
        return;
    }
    // check if WYSIWYG is enabled
    if ( 'true' == get_user_option( 'rich_editing' ) ) {
        add_filter( 'mce_external_plugins', 'my_add_tinymce_plugin' );
        add_filter( 'mce_buttons', 'my_register_mce_button' );
    }
}
add_action('admin_head', 'my_add_mce_button');
function my_add_tinymce_plugin( $plugin_array ) {
    $plugin_array['my_mce_button'] = get_template_directory_uri() .'/js/editor_plugin.js';
    return $plugin_array;
}
function my_register_mce_button( $buttons ) {
    array_push( $buttons, 'my_mce_button' );
    return $buttons;
}
function wpa_47010( $qtInit ) {
    $qtInit['buttons'] = 'strong,em,link,spell,more';
    return $qtInit;
}
add_filter('quicktags_settings', 'wpa_47010');

/** CUSTOM SHORTCODES **/
/* CUADRO DE OPINION A LA DERECHA */
add_shortcode( 'opinion_derecha', 'op_derecha' );
function op_derecha( $atts ) {
    $atts = shortcode_atts(array('foto' => '\"\"', 'nombre' => '\"\"', 'contenido' => '\"\"' ), $atts );
    return '<div class="media"><div class="media-body"><h4 class="media-heading text-right">' . $atts['nombre'] . '</h4><p>' . $atts['contenido'] . '</p></div><div class="media-right"><a><img class="media-object" src="' . $atts['foto'] . '" alt="sumarium" /></a></div></div><div class="clearfix"></div>';
}

/* CUADRO DE OPINION A LA DERECHA */
add_shortcode( 'opinion_izquierda', 'op_izquierda' );
function op_izquierda( $atts ) {
    $atts = shortcode_atts( array('foto' => '\"\"', 'nombre' => '\"\"', 'contenido' => '\"\"'), $atts );
    return '<div class="media"><div class="media-left"><a><img class="media-object" src="' . $atts['foto'] . '" alt="sumarium" /></a></div><div class="media-body"><h4 class="media-heading">' . $atts['nombre'] . '</h4><p>' . $atts['contenido'] . '</p></div></div><div class="clearfix"></div>';
}

add_shortcode( 'cuadro_pre', 'pre' );
function pre( $atts , $content = null ) {
    return '<pre>' . do_shortcode($content) . '</pre>';
}

add_shortcode( 'quote_izq', 'quote_izq' );
function quote_izq( $atts , $content = null ) {
    return '<aside class="asideleft">' . do_shortcode($content) . '</aside>';
}


add_shortcode( 'quote_der', 'quote_der' );
function quote_der( $atts , $content = null ) {
    return '<aside class="asideright">' . do_shortcode($content) . '</aside>';
}

add_shortcode( 'alinear_izq', 'alinear_izq' );
function alinear_izq( $atts , $content = null ) {
    return '<div class="alinearder">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'alinear_centro', 'alinear_centro' );
function alinear_centro($attr,$content){
    $anchor = $attr['ancho'];
    $prem = (1170 - $anchor) / 2;
    $margen = ($prem * 100) /1170;
    $margen = round($margen, 2);
    $mediaq = $margen / 2;
    return "<style type='text/css'>@media(max-width: 1190px){.centrado {margin-left:{$mediaq}% !important;}} @media(max-width: 991px){.centrado {margin-left:0% !important;}}</style><div class='clearfix'></div><div class='centrado' style='margin-left:{$margen}%'>" . do_shortcode($content) . "</div><div class='clearfix'></div>";
}

add_shortcode( 'alinear_der', 'alinear_der' );
function alinear_der( $atts , $content = null ) {
    return '<div class="alinearizq pull-right">' . do_shortcode($content) . '</div>';
}

/* 2 COLUMNAS */
add_shortcode( 'columna2_inicio', 'columna2_inicio' );
function columna2_inicio( $atts , $content = null ) {
    return '<div class="columna2 col-md-6 no-paddingl">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'columna2_final', 'columna2_final' );
function columna2_final( $atts , $content = null ) {
    return '<div class="columna2 col-md-6 no-paddingr">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}

/* 3 COLUMNAS */
add_shortcode( 'columna3_inicio', 'columna3_inicio' );
function columna3_inicio( $atts , $content = null ) {
    return '<div class="columna3 col-md-4 no-paddingl">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'columna3_medio', 'columna3_medio' );
function columna3_medio( $atts , $content = null ) {
    return '<div class="columna3 col-md-4 no-paddingl">' . do_shortcode($content) . '</div>';
}
add_shortcode( 'columna3_final', 'columna3_final' );
function columna3_final( $atts , $content = null ) {
    return '<div class="columna3 col-md-4 no-paddingr">' . do_shortcode($content) . '</div><div class="clearfix"></div>';
}


/* RESALTADO */
add_shortcode( 'resaltado', 'resaltado' );
function resaltado( $atts , $content = null ) {
    return '<div class="resaltado">' . do_shortcode($content) . '</div>';
}

/* GRANATE */
add_shortcode( 'granate', 'granate' );
function granate( $atts , $content = null ) {
    return '<div class="granate">' . do_shortcode($content) . '</div>';
}


add_shortcode( 'embed_infografia', 'embed_infografia' );
function embed_infografia($attr,$content = null){
    return '<div class="embed-infografia">' . do_shortcode($content) . '</div>';
}

/**REMOVE AUTO P BR*/

add_filter("the_content", "the_content_filter");
function the_content_filter($content) {
    // array of custom shortcodes requiring the fix
    $block = join("|",array("col","resaltado","granate"));
    // opening tag
    $rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);

    // closing tag
    $rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);
    return $rep;
}

/* MARCO IMAGEN */
add_shortcode( 'marco_imagen', 'marco_imagen' );
function marco_imagen($attr,$content){
    $anchor = $attr['ancho'];
    if ($anchor >= 1170){
        $anchor = 1170;
    } else {
        $anchor = $attr['ancho'] + 22;
    }
    return "<div class='img-frame' style='width:{$anchor}px'><img src='{$attr['foto']}' alt='{$attr['leyenda']}' width='{$attr['ancho']}' height='{$attr['alto']}' /><div class='clearfix'></div><div class='caption-frame col-md-12 no-paddingl no-paddingr' style='max-width:{$attr['ancho']}px'><p>{$content}</p></div></div>";
}

/* MARCO VIDEO
add_shortcode( 'marco_video', 'marco_video' );
function marco_video($atts = shortcode_atts(array('leyenda' => '\"\"' ), $atts ), $content = null ) {
    return '<div class="iframe-bs">' . $content . '<div class="clearfix"></div><div class="caption-frame col-md-12 no-paddingl no-paddingr"><p>' . $atts['leyenda'] . '</p></div></div>';
} */

/* MARCO VIDEO*/
add_shortcode('marco_video', 'marco_video');
function marco_video($attr,$content){
    $anchor = $attr['ancho'] + 22;
    return "<div class='iframe-bs' style='width:{$anchor}px' >{$content}<div class='clearfix'></div><div class='caption-frame col-md-12 no-paddingl no-paddingr'><p>{$attr['leyenda']}</p></div></div>";
}

/** CUSTOM QUICKTAGS **/
function custom_quicktags() {
    if ( wp_script_is( 'quicktags' ) ) {
?>
<script type="text/javascript">
    QTags.addButton( 'eg_h2', 'h2', '<h2>', '</h2>', 'h2', 'H2', 3 );
    QTags.addButton( 'eg_pre', 'Texto Cuadro Pre', '[cuadro_pre]', '[/cuadro_pre]', 'pre', 'Texto Importante en Cuadro', 111 );
    QTags.addButton( 'eg_suquote', 'Quote Centrada', '[su_quote]', '[/su_quote]', 'suquote', 'Quote centrada', 112 );
    QTags.addButton( 'eg_paragraph', 'Marco Imagen', '[marco_imagen foto="" ancho="1170" alto=""]--LEYENDA--[/marco_imagen]', '', 'image-frame', 'Marco para Imagen', 113 );
    QTags.addButton( 'eg_video', 'Marco Video', '<div class="iframe-bs">|--IFRAME--|<div class="clearfix"></div><div class="caption-frame col-md-12 no-paddingl no-paddingr" style="max-width:|--ANCHO DEL IFRAME--|px"><p>|--LEYENDA--|</p></div></div>', '', 'video', 'Marco para video/media con leyenda', 114 );
    QTags.addButton( 'eg_qdi', 'Quote Destacado Izq', '[quote_izq]', '[/quote_izq]', 'Quote Destacado Izq', 'Quote Destacado alineado a la Izquierda', 115 );
    QTags.addButton( 'eg_qdd', 'Quote Destacado Der', '[quote_der]', '[/quote_der]', 'Quote Destacado Der', 'Quote Destacado alineado a la Derecha', 116 );
    QTags.addButton( 'eg_resalt', 'Resaltado', '[resaltado]', '[/resaltado]', 'Resaltado', 'Resaltado', 120 );
    QTags.addButton( 'eg_granat', 'Resaltado - Granate', '[granate]', '[/granate]', 'Resaltado - Granate', 'Resaltado - Granate', 121 );
    QTags.addButton( 'eg_hr', 'Linea Horizontal', '<hr />', '', 'h', 'Inserta Linea Horizontal', 201 );
    QTags.addButton( 'eg_ai', 'Alinear Izq', '[alinear_izq]', '[/alinear_izq]', 'Alinear izq', 'Alineado Izq', 202 );
    QTags.addButton( 'eg_ac', 'Alinear Centro', '[alinear_centro ancho="1170"]', '[/alinear_centro]', 'Alinear Centro', 'Alineado Centro', 203 );
    QTags.addButton( 'eg_ad', 'Alinear Der', '[alinear_der]', '[/alinear_der]', 'Alinear der', 'Alineado Der', 204 );
    QTags.addButton( 'eg_col6', 'Abrir 2-col', '[columna2_inicio]', '[/columna2_inicio]', '2 Columnas', '2 Columnas', 205 );
    QTags.addButton( 'eg_col6f', 'Cerrar 2-col', '[columna2_final]', '[/columna2_final]', 'Cerrar 2 Columnas', 'Cerrar 2 Columnas', 206 );
    QTags.addButton( 'eg_col4', 'Abrir 3-col', '[columna3_inicio]', '[/columna3_inicio]', '3 Columnas', '3 Columnas', 207 );
    QTags.addButton( 'eg_col4m', 'Medio 3-col', '[columna3_medio]', '[/columna3_medio]', 'Medio 3 Columnas', 'Medio 3 Columnas', 208 );
    QTags.addButton( 'eg_col4f', 'Cerrar 3-col', '[columna3_final]', '[/columna3_final]', 'Cerrar 3 Columnas', 'Cerrar 3 Columnas', 209 );
    QTags.addButton( 'eg_carai', 'Opinion Lado Izq.', '<div class="media"><div class="media-left"><a><img class="media-object" src="|--FOTO 200x200--|" alt="sumarium" /></a></div><div class="media-body"><h4 class="media-heading">|--NOMBRE--|</h4><p>|--CONTENIDO--|</p></div></div><div class="clearfix"></div>', '', 'Opinion Lado Izq.', 'Opinion Lado Izq.', 210 );
    QTags.addButton( 'eg_carad', 'Opinion Lado Der.', '<div class="media"><div class="media-body"><h4 class="media-heading text-right">|--NOMBRE--|</h4><p>|--CONTENIDO--|</p></div><div class="media-right"><a><img class="media-object" src="|--FOTO 200x200--|" alt="sumarium" /></a></div></div><div class="clearfix"></div>', '', 'Opinion Lado Der.', 'Opinion Lado Der.', 211 );
    QTags.addButton( 'eg_ei', 'Embed para Infografia', '[embed_infografia]<iframe class="iframe-info" src="http://localhost/wp_sumarium/embed-infografia/?t=ID" frameborder="0"></iframe>', '[/embed_infografia]', 'Embed para Infografia', 'Embed para Infografia', 212 );
</script>
<?php
    }
}
add_action( 'admin_print_footer_scripts', 'custom_quicktags' );

/* --------------------------------------------------------------
    CUSTOM FUNCTIONS - EXCERPT - TIME - SANITIZE
-------------------------------------------------------------- */

/* CUSTOM EXCERPT */
function get_excerpt($count){
    $foto = 0;
    $permalink = get_permalink($post->ID);
    $category = get_taxonomies($post->ID);
    $excerpt = get_the_excerpt($post->ID);
    if ($excerpt == ""){
        $excerpt = get_the_content($post->ID);
    }
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt.'... <a class="plus" href="'.$permalink.'">+</a>';
    return $excerpt;
}

/** CUSTOM EXCERPT ESPECIAL **/
function get_excerpt_special($count){
    $foto = 0;
    $permalink = get_permalink($post->ID);
    $category = get_taxonomies($post->ID);
    $excerpt = get_the_excerpt($post->ID);
    if ($excerpt == ""){
        $excerpt = get_the_content($post->ID);
    }
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt.'... <a class="plus-special" href="'.$permalink.'">Leer Mas</a>';
    return $excerpt;
}

/* GET EXCERPT POR ID - PARA HEADER DESCRIPTION META */
function get_excerpt_by_id($post_id){
    $the_post = get_post($post_id); //Gets post ID
    $the_excerpt = $the_post->post_content; //Gets post_content to be used as a basis for the excerpt
    $excerpt_length = 20; //Sets excerpt length by word count
    $the_excerpt = strip_tags(strip_shortcodes($the_excerpt)); //Strips tags and images
    $words = explode(' ', $the_excerpt, $excerpt_length + 1);

    if(count($words) > $excerpt_length) :
        array_pop($words);
        array_push($words, '…');
        $the_excerpt = implode(' ', $words);
    endif;

    return $the_excerpt;
}

/** FUNCION PARA COLOCAR TIEMPO EN ENTRADAS **/
function themeblvd_time_ago() {
    global $post;
    $date = get_post_time('G', true, $post);
    $chunks = array(
        array( 60 * 60 * 24 * 365 , __( 'año', 'themeblvd' ), __( 'años', 'themeblvd' ) ),
        array( 60 * 60 * 24 * 30 , __( 'mes', 'themeblvd' ), __( 'meses', 'themeblvd' ) ),
        array( 60 * 60 * 24 * 7, __( 'semana', 'themeblvd' ), __( 'semanas', 'themeblvd' ) ),
        array( 60 * 60 * 24 , __( 'dia', 'themeblvd' ), __( 'dias', 'themeblvd' ) ),
        array( 60 * 60 , __( 'hora', 'themeblvd' ), __( 'horas', 'themeblvd' ) ),
        array( 60 , __( 'minuto', 'themeblvd' ), __( 'minutos', 'themeblvd' ) ),
        array( 1, __( 'segundo', 'themeblvd' ), __( 'segundos', 'themeblvd' ) )
    );
    if ( !is_numeric( $date ) ) {
        $time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
        $date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
        $date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
    }
    $current_time = current_time( 'mysql', $gmt = 0 );
    $newer_date = time( );
    $since = $newer_date - $date;
    if ( 0 > $since )
        return __(  ' un momento', 'themeblvd' );
    for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
        $seconds = $chunks[$i][0];
        if ( ( $count = floor($since / $seconds) ) != 0 )
            break;
    }
    $output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
    if ( !(int)trim($output) ){
        $output = '0 ' . __( 'segundos', 'themeblvd' );
    }
    return $output;
}

/** QUITAR ACENTOS Y CARACTERES ESPECIALES - USADO PARA SLUGS DE CATEGORIAS **/
function normaliza ($cadena){
    $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ
ßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
    $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuy
bsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
    $cadena = utf8_decode($cadena);
    $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
    $cadena = strtolower($cadena);
    return utf8_encode($cadena);
}

?>
