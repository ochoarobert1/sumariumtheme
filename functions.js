var ver = false, tagvisible = true, tagheight = 0;
$(window).load(function () {
    "use strict";
    $("body").niceScroll({
        autohidemode: 'false',
        background: '#939393',
        cursorwidth: '5px',
        cursorcolor: '#C70707'
    });
    $('#tags-content').niceScroll({
        background: '#939393',
        cursorwidth: '5px',
        cursorcolor: '#C70707'
    });
});
$(document).ready(function () {
    "use strict";
    $("#top-navbar").sticky({topSpacing: 0});
    $("#sticker-sidebar").sticky({topSpacing: 70, bottomSpacing: 620});
    $("#top-navbar-mobile").sticky({topSpacing: 0});
    setTimeout(function () {
        tagheight = $('#content-bar').height() + 400;
        $('#tags-bar').height(tagheight);
    }, 2000);
    setTimeout(function () {
        tagheight = $('#content-bar').height() + 400;
        $('#tags-bar2').height(tagheight);
    }, 2000);
    /* $("#search").validationEngine(); */
    var owl = $("#owl-opinions");
    owl.owlCarousel({
        items : 5,
        pagination : false,
        navigation : true,
        navigationText : ["<span class='glyphicon glyphicon-chevron-left'></span>", "<span class='glyphicon glyphicon-chevron-right'></span>"]
    });
    /* setTimeout(function() {
        location.reload();
    }, 300000); */
});
// Opacity Effect
function opacitynav() {
    "use strict";
    if (ver === false) {
        $('#top-navbar').removeClass('opacity1');
        $('#top-navbar').addClass('opacity2');
    } else {
        $('#top-navbar').addClass('opacity1');
        $('#top-navbar').removeClass('opacity2');
    }
}


$('#tags-wrapper').click(function () {
    "use strict";
    if (tagvisible === true) {
        $('#tags-bar').addClass('col-md-1');
        $('#tags-bar').removeClass('col-md-2');
        $('#tags-bar').addClass('tags-mini-container');
        $('#tags-bar').removeClass('tags-main-container');
        $('.tags-section-title2').removeClass('showdiv');
        $('.tags-section-title2').addClass('hidediv');
        $('#sidebar-tag-nav').removeClass('col-md-2');
        $('#sidebar-tag-title').removeClass('col-md-10');
        $('#sidebar-tag-nav').addClass('col-md-12');
        $('#sidebar-tag-title').addClass('col-md-12');
        tagvisible = false;
    } else {
        $('#tags-bar').removeClass('col-md-1');
        $('#tags-bar').addClass('col-md-2');
        $('#tags-bar').removeClass('tags-mini-container');
        $('#tags-bar').addClass('tags-main-container');
        $('#sidebar-tag-nav').removeClass('col-md-12');
        $('#sidebar-tag-title').removeClass('col-md-12');
        $('#sidebar-tag-nav').addClass('col-md-2');
        $('#sidebar-tag-title').addClass('col-md-10');
        setTimeout(function () {
            $('.tags-section-title2').removeClass('hidediv');
            $('.tags-section-title2').addClass('showdiv');
        }, 2000);
        tagvisible = true;
    }
});



$('#tags-xs-wrapper').click(function () {
    $('#tags-bar2').removeClass('tags-mini-container2');
    $('#tags-bar2').addClass('tags-main-container');
    $('#sidebar-tag-nav').removeClass('col-md-12');
    $('#sidebar-tag-title').removeClass('col-md-12');
    $('#sidebar-tag-nav').addClass('col-md-2');
    $('#sidebar-tag-title').addClass('col-md-10');
    setTimeout(function () {
        $('.tags-section-title2').removeClass('hidediv');
        $('.tags-section-title2').addClass('showdiv');
    }, 2000);
    $('#tags-xs-wrapper').removeClass('showdiv');
    $('#tags-xs-wrapper').addClass('hidediv');
});

$('#tags-wrapper-xs').click(function () {
    $('#tags-bar2').removeClass('tags-main-container');
    $('#tags-bar2').addClass('tags-mini-container2');
    $('#sidebar-tag-nav').removeClass('col-md-12');
    $('#sidebar-tag-title').removeClass('col-md-12');
    $('#sidebar-tag-nav').addClass('col-md-2');
    $('#sidebar-tag-title').addClass('col-md-10');
    setTimeout(function () {
        $('.tags-section-title2').removeClass('hidediv');
        $('.tags-section-title2').addClass('showdiv');
    }, 2000);
    $('#tags-xs-wrapper').removeClass('hidediv');
    $('#tags-xs-wrapper').addClass('showdiv');
});




$(window).scroll(function () {
    "use strict";
    var window_top = $(window).scrollTop();
    if (window_top > 10) {
        ver = false;
    } else {
        ver = true;
    }
    opacitynav();
});

smoothScroll.init({
    speed: 1000,
    easing: 'easeInOutCubic',
    offset: 100,
    updateURL: false
});


/* CAMBIAR DE NOTA EN TAG TEMPLATE */
function show_taged(x, y) {
    "use strict";
    // ruta (variable de template url)
    var item = x, ant = y;
    $.ajax({
        type : 'POST',
        url : ruta + '/templates/show-tag-function.php',
        data: {
            id: item
        },
        beforeSend: function () {
            $('#tag_frame').html(
                '<div class="col-md-12 text-center">' + '<p>Cargando<p>' + '<img src="' + ruta + '/images/AjaxTagLoader.gif">' + '</div>'
            );
        },
        success: function (resp) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#tag_frame').html(resp);
            $(".tag-news-title2 > *").removeClass('activo');
            $('#title-' + x).addClass('activo');
            setTimeout(function () {
                tagheight = $('#tag_frame').height() + 300;
                $('#tags-bar').height(tagheight);
            }, 2000);
        }
    });
}

function show_taged_xs(x, y) {
    "use strict";
    // ruta (variable de template url)
    var item = x, ant = y;
    $.ajax({
        type : 'POST',
        url : ruta + '/templates/show-tag-function.php',
        data: {
            id: item
        },
        beforeSend: function () {
            $('#tag_frame').html(
                '<div class="col-md-12 text-center">' + '<p>Cargando<p>' + '<img src="' + ruta + '/images/AjaxTagLoader.gif">' + '</div>'
            );
        },
        success: function (resp) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            $('#tag_frame').html(resp);
            $(".tag-news-title2 > *").removeClass('activo');
            $('#title-' + x).addClass('activo');
            setTimeout(function () {
                tagheight = $('#tag_frame').height() + 300;
                $('#tags-bar2').height(tagheight);
            }, 2000);
            $('#tags-bar2').removeClass('tags-main-container');
            $('#tags-bar2').addClass('tags-mini-container2');
            $('#sidebar-tag-nav').removeClass('col-md-12');
            $('#sidebar-tag-title').removeClass('col-md-12');
            $('#sidebar-tag-nav').addClass('col-md-2');
            $('#sidebar-tag-title').addClass('col-md-10');
            setTimeout(function () {
                $('.tags-section-title2').removeClass('hidediv');
                $('.tags-section-title2').addClass('showdiv');
            }, 2000);
            $('#tags-xs-wrapper').removeClass('hidediv');
            $('#tags-xs-wrapper').addClass('showdiv');
        }
    });
}

//Funciones del single...
function next(x) {
    "use strict";
    $.ajax({
        type: 'POST',
        url: ruta + '/templates/show-foto-function.php',
        beforeSend: function () {
            $('#fotos-wrapper').html('<div style="text-align: center; margin-top:15px;"><img src="' + ruta + '/images/AjaxLoader.gif"></div>');
        },
        data: {
            id: x
        },
        success: function (resp) {
            $('#fotos-wrapper').html(resp);
        }
    });
}

//Funciones del single...
function prev(x) {
    "use strict";
    $.ajax({
        type: 'POST',
        url: ruta + '/templates/show-foto-function.php',
        beforeSend: function () {
            $('#fotos-wrapper').html('<div style="text-align: center; margin-top:15px;"><img src="' + ruta + '/images/AjaxLoader.gif"></div>');
        },
        data: {
            id: x
        },
        success: function (resp) {
            $('#fotos-wrapper').html(resp);
        }
    });
}
